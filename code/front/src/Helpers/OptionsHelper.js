
let stateOptions = function getStateOptions() {
    return [
        {label:"Open", value: "Open"},
        {label: "In progress", value: "In progress"},
        {label: "Blocked", value: "Blocked"},
        {label: "Resolved", value: "Resolved"},
        {label: "Closed", value: "Closed"}
    ];
}

let permissionOptions = function getPermissionOptions() {
    return [
            {label: "None", value:"0"},
            {label: "Admin", value: "7"},
            {label: "View and edit", value: "6"},
            {label: "View and delete", value: "5"},
            {label: "View", value: "4"},
        ];
}

export {
    stateOptions as StateOptions,
    permissionOptions as PermissionOptions
};
