import Axios from "axios";

const axiosConfig ={
    baseURL : 'http://localhost:8000',
    headers:{ Authorization: localStorage.getItem('token')},
};

let actions = {
    getNote :  (noteId) => {
        return Axios.get('/notes/'+noteId, axiosConfig)
            .then((response) => {
                if (response.data.hasOwnProperty('note')) {
                    return response.data.note;

                }
            })
            .catch((errors) => {
                console.log('Error in edit occurred');
                console.log(errors);
                return null;
            });
    },

    editProject : (projectId, inputData) => {
        return Axios.put('/projects/'+projectId, inputData, axiosConfig)
            .then((response) => {
                return 'OK';
            })
            .catch((errors) => {
                return errors;
            });
    },

};

export default actions;