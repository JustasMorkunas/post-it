import React, { Component } from 'react';
import './styles/app.scss';
import AuthPanel from './Containers/AuthPanel';
import LoggedPanel from './Containers/LoggedPanel';
import {CircularProgress} from '@material-ui/core'
import Axios from "axios";

class App extends Component {

    constructor(props){
        super(props);
        this.state = {
            isLogged : false,
            showSpinner: true,
        };
    }

    changeLoggedState = () => {
        let tempState = this.state;
        tempState.isLogged = true;
        this.setState(tempState);
    };

    openPanel = () => {
        if (!this.state.showSpinner) {
            if (this.state.isLogged) {
                return <LoggedPanel changeLoggedState={this.changeLoggedState}/>;
            }
            return <AuthPanel changeLoggedState={this.changeLoggedState}/>
        }
        return <CircularProgress/>;
    };

    validateToken = () => {
        let token = localStorage.getItem('token');
        let tempState = this.state;
        if(token){
            Axios.get('/validate/user', {
                baseURL:'http://localhost:8000/',
                headers: { Authorization: token },
                method: 'GET',
            }).then(() => {
                tempState.isLogged = true;
                tempState.showSpinner =false;
                this.setState(tempState);
            })
                .catch(() => {
                    tempState.isLogged = false;
                    tempState.showSpinner = false;
                    this.setState(tempState);
                });
        } else {
            tempState.isLogged = false;
            tempState.showSpinner = false;
            this.setState(tempState);
        }
    };

    componentDidMount(){
        this.validateToken();
    }

  render() {
    return (
      <div className="App">
          <div className="panel-md">
              {this.openPanel()}
          </div>
      </div>
    );
  }
}

export default App;
