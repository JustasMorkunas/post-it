import React from 'react';
import {Grid, Typography, Menu, MenuItem, withStyles, Fab} from "@material-ui/core";
import {Edit, Delete, Folder} from '@material-ui/icons'
import NewProjectDialog from '../Components/NewProjectDialog';
import Axios from "axios";

const styles = theme => ({
    root: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.paper,
    },
});

const resourceAxios = Axios.create({
    baseURL : 'http://localhost:8000',
    headers:{ Authorization: localStorage.getItem('token')},
});

class ProjectElement extends React.Component{

    constructor(prop){
        super(prop);
        this.state = {
            project: this.props.projectInfo,
            menu:{open:false},
            anchorEl: null,
        };
    }

    componentDidUpdate(){
        if (
            this.state.project.id != this.props.projectInfo.id ||
            this.state.project.title != this.props.projectInfo.title
        ){
            let tempState = this.state;
            tempState.project = this.props.projectInfo;
            tempState.menu.open = false;
            this.setState(tempState);
        }
    }

    handleMenuOpen = (e) => {
        e.preventDefault();
        let tempState = this.state;
        tempState.menu.open = true;
        tempState.anchorEl = e.currentTarget;
        this.setState(tempState);
    };

    handleMenuClose = () => {
        let tempState = this.state;
        tempState.menu.open = false;
        tempState.anchorEl = null;
        this.setState(tempState);
    };

    deleteProject = () => {
        resourceAxios.post('/projects/'+this.state.project.id, {'_method':'DELETE'})
            .then((response)=>{
                this.props.updateContents();
                this.handleMenuClose();
            }).catch((error) => {
                console.log("Delete project error occurred");
                console.log(error.response);
            });
    };

    render(){
        const { classes } = this.props;
        const { anchorEl } = this.state;

        return(
            <Grid className="project-item" item xs={2} onContextMenu={(e)=>{this.handleMenuOpen(e)}}  aria-haspopup="true">
                <Folder fontSize="large" color="secondary" aria-controls="simple-menu" onClick={this.props.openProject.bind(this, this.state.project.id)}/>
                <Typography className={classes.title}
                            color="textSecondary"
                            onClick={this.props.openProject.bind(this, this.state.project.id)}
                            key={this.state.project.id}
                            gutterBottom> {this.state.project.title}
                            </Typography>

                    <Menu anchorEl={anchorEl} id="simple-menu" open={this.state.menu.open} onClose={this.handleMenuClose}>
                        <MenuItem >
                            <NewProjectDialog projectId={this.props.getProjectId}
                                              projectData={this.state.project}
                                              changeLoggedState={this.props.changeLoggedState}
                                              refreshContent={this.props.updateContents}
                                              buttonElement={<div><Edit fontSize={"small"} style={{paddingRight:"10px"}}/>Edit</div>}
                            />

                        </MenuItem>
                        <MenuItem onClick={this.deleteProject}><Delete fontSize={"small"} style={{paddingRight:"10px"}}/>Delete</MenuItem>
                    </Menu>

            </Grid>
        );

    }

}

export default withStyles(styles)(ProjectElement);