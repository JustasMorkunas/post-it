import React from 'react';
import {AccountCircle, Contacts, Email, Face} from '@material-ui/icons'
import Axios from "axios";
import {Button, Dialog, FormHelperText, Grid, TextField, withStyles} from "@material-ui/core";
import cloneDeep from 'lodash/cloneDeep';

const axiosConfig ={
    baseURL : 'http://localhost:8000',
    headers:{ Authorization: localStorage.getItem('token')},
};

const styles = theme => ({
    margin: {
        margin: theme.spacing.unit * 2,
    },
    padding: {
        padding: theme.spacing.unit
    }
});

class ProfileData extends React.Component {
    
    constructor(props) {
        super(props);
        this.state = {
            user: {
                name: "",
                lastname: "",
                email: "",
            },
            dialog: {
                open: false,
            },
            inputs:{
                name: '',
                lastname: '',
                email: '',
            },
            errors: {
                name: {status: false, message: ''},
                lastname: {status: false, message: ''},
                email: {status: false, message: ''},
            }
        };
    }

    componentDidMount(){
        this.getProfileData().then(() => {
            this.state.inputs = cloneDeep(this.state.user);
            this.setState(this.state);
        });
    }

    handleClose = () => {
        this.state.dialog.open = false;
        this.setState(this.state);
    };

    handleOpen = () => {
        this.state.dialog.open = !this.state.dialog.open;
        this.setState(this.state);
    };

    handleInputChange = (inputName, value) => {
        var tempState = this.state;
        tempState.errors[inputName].status = false;
        tempState.errors[inputName].message = '';
        tempState.inputs[inputName] = value;
        this.setState(tempState);
    };

    getProfileData = () => {
        return Axios.get('/user', axiosConfig)
            .then((response) => {
                this.state.user = response.data.user;
                this.setState(this.state);
            })
    };

    submitChanges = () => {
        Axios.post('/user/'+this.state.user.id, this.state.inputs, axiosConfig)
            .then((response) => {
                console.log(response);
                if (response.status == 200) {
                    this.state.user = response.data.user;
                    this.handleClose();
                } else {
                    for(var property in response.data){
                        this.state.errors[property].status = true;
                        this.state.errors[property].message = response.data[property][0];
                    }
                }
                this.setState(this.state);
            });
    };

    render() {
        const { classes } = this.props;
        return(
            <div className="profile-block">
                <div className="menu-item">
                    <Button style={{color:"white"}} onClick={this.handleOpen}>
                        <AccountCircle className="profile-ic"/>
                        <span className="profile-txt">{this.state.user.name} {this.state.user.lastname}</span>
                    </Button>
                </div>

                <Dialog open={this.state.dialog.open} onClose={this.handleClose} aria-labelledby="form-dialog-title">
                    <div className={classes.margin}>
                        <div>
                        <Grid container spacing={8} alignItems="flex-end">
                            <Grid item>
                                <Face />
                            </Grid>
                            <Grid item md={true} sm={true} xs={true}>
                                <TextField error={this.state.errors.name.status} value={this.state.inputs.name} id="name" label="Name" type="text" onChange={(e) =>{this.handleInputChange('name', e.target.value)}} fullWidth autoFocus required />
                            </Grid>
                        </Grid>
                        <FormHelperText error={true}>{this.state.errors.name.message}</FormHelperText>
                        </div>
                        <div>
                        <Grid container spacing={8} alignItems="flex-end">
                            <Grid item>
                                <Contacts/>
                            </Grid>
                            <Grid item md={true} sm={true} xs={true}>
                                <TextField error={this.state.errors.lastname.status} value={this.state.inputs.lastname} id="lastname" label="Lastname" onChange={(e) =>{this.handleInputChange('lastname', e.target.value)}} type="text" fullWidth autoFocus required />
                            </Grid>
                        </Grid>
                        <FormHelperText error={true}>{this.state.errors.lastname.message}</FormHelperText>
                        </div>
                        <div>
                        <Grid container spacing={8} alignItems="flex-end">
                            <Grid item>
                                <Email/>
                            </Grid>
                            <Grid item md={true} sm={true} xs={true}>
                                <TextField error={this.state.errors.email.status} value={this.state.inputs.email} id="email" label="Email" type="email" onChange={(e) =>{this.handleInputChange('email', e.target.value)}} fullWidth autoFocus required />
                            </Grid>
                        </Grid>
                        <FormHelperText error={true}>{this.state.errors.email.message}</FormHelperText>
                        </div>
                        <Grid container justify="center" style={{ marginTop: '10px' }}>
                            <Button variant="outlined"
                                    color="primary"
                                    style={{ textTransform: "none" }}
                                    onClick={this.submitChanges}
                            >Save</Button>
                        </Grid>
                    </div>
                </Dialog>
            </div>
        )
    }
}

export default withStyles(styles) (ProfileData);