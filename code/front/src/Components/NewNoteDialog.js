import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import OptionsSelector from './OptionsSelector';
import SharedWithComponent from '../Components/SharedWithComponent';
import {StateOptions, PermissionOptions} from '../Helpers/OptionsHelper';
import Actions from '../Helpers/RequestActions';
import Axios from "axios";

const axiosConfig ={
    baseURL : 'http://localhost:8000',
    headers:{ Authorization: localStorage.getItem('token')},
};

class NewNoteDialog extends React.Component{

    constructor(props) {
        super(props);
        let stateDefaults = {
            open: false,
            inputData: {
                project_id: 0,
                status: 'Open',
                title: '',
                content: '',
                shared_with:[],
                access_permissions: '0'
            },
            errors: {
                status: {status: false, message: ''},
                title: {status: false, message: ''},
                content: {status: false, message: ''},
            },
            sharedWithError: false,
        };

        if(this.props.hasOwnProperty('stateData')){
            for(var prop in this.props.stateData){
                stateDefaults.inputData[prop] = this.props.stateData[prop];
            }
        }
        this.state = stateDefaults;
    }

    componentDidUpdate(prevProps){
        if(this.props.hasOwnProperty('stateData') && prevProps.hasOwnProperty('stateData')){
            if(JSON.stringify(this.props.stateData) !== JSON.stringify(prevProps.stateData)) {
                let tempState = this.state;
                for(var prop in this.props.stateData){
                    tempState.inputData[prop] = this.props.stateData[prop];
                }
                this.setState(tempState);
            }
        }
    }

    handleOpen = () => {
        let tempState = this.state;
        tempState.open = true;
        if (this.props.hasOwnProperty('noteId')) {
            Actions.getNote(this.props.noteId).then((response) => {
                if (response != null) {
                    for(var prop in tempState.inputData){
                        tempState.inputData[prop] = response[prop];
                    }
                }
                console.log(tempState);
                this.setState(tempState);
            });
        } else {
            this.setState(tempState);
        }
    };

    handleClose = () => {
        let tempState = this.state;
        tempState.open = false;
        this.setState(tempState);
    };

    handleInputChange = (inputName, value) => {
        let tempState = this.state;
        if(tempState.inputData.hasOwnProperty(inputName)){
            tempState.inputData[inputName] = value;
            tempState.errors[inputName].state = false;
            tempState.errors[inputName].message = '';
        }
        this.setState(tempState);
    };

    handleInputErrors = (errors) => {
        let tempState = this.state;
        tempState.sharedWithError = false;
        for(let prop in errors){
            if (/shared_with\.\d/.test(prop)) {
                tempState.sharedWithError = true;
            }
            if(tempState.errors.hasOwnProperty(prop)){
                tempState.errors[prop].status = true;
                tempState.errors[prop].message = errors[prop][0];
            }
            
        }
        console.log(tempState);
        this.setState(tempState);
    };

    handleNoteSave = () => {
        let tempState = this.state;
        tempState.inputData.project_id = this.props.projectId();
        this.setState(tempState);
        Axios.post('/notes', this.state.inputData, axiosConfig)
            .then((response) => {
                this.handleClose();
                this.props.refreshContent();
            })
            .catch((errors) => {
                this.handleNoteSaveErrors(errors)
            });
    };

    handleNoteEdit = () => {
        let tempState = this.state;
        if (this.props.projectId() != 'shared') {
            tempState.inputData.project_id = this.props.projectId();
        }
        this.setState(tempState);
        console.log(this.state.inputData);
        Axios.put('/notes/'+this.props.noteId, this.state.inputData, axiosConfig)
            .then((response) => {
                this.handleClose();
                this.props.refreshContent();
            })
            .catch((errors) => {
                console.log('Error in edit occurred');
                console.log(errors);
                this.handleNoteSaveErrors(errors)
            });
    };

    handleNoteSaveErrors = (errors) => {
        console.log(errors.response);
        switch (errors.response.status) {
            case 401:
                this.handleClose();
                this.props.changeLoggedState();
                break;
            case 400:
                this.handleInputErrors(errors.response.data);
                break;
        }
    };

    handleSaveBtnClick = () => {
        if(this.props.hasOwnProperty('noteId'))
            this.handleNoteEdit();
        else
            this.handleNoteSave();
    };

    handleStatusUpdate = (value) => {
        this.state.inputData.status = value;
        this.setState(this.state);
    };

    handlePermissionsUpdate = (value) => {
        this.state.inputData.access_permissions = value;
        this.setState(this.state);
    };

    handleSharedWithUpdate = (values) => {
      this.state.inputData.shared_with = values;
      this.setState(this.state);

    };

    render(){
        return (
            <div className="menu-item">
                <div onClick={this.handleOpen}>
                    {this.props.buttonElement}
                </div>
                <Dialog open={this.state.open} onClose={this.handleClose} aria-labelledby="form-dialog-title">
                    <DialogTitle id="form-dialog-title">{this.props.hasOwnProperty('noteId') ? "Edit post-it" : "Create new post-it"}</DialogTitle>
                    <DialogContent >
                        <TextField
                            autoFocus
                            margin="dense"
                            id="title"
                            label="Note title"
                            type="text"
                            fullWidth
                            error={this.state.errors.title.status}
                            value={this.state.inputData.title}
                            onChange={(e) => {this.handleInputChange('title', e.target.value)}}
                        />
                        <TextField
                            autoFocus
                            margin="dense"
                            id="content"
                            label="Note content"
                            type="text"
                            fullWidth
                            multiline
                            error={this.state.errors.content.status}
                            value={this.state.inputData.content}
                            onChange={(e) => {this.handleInputChange('content', e.target.value)}}
                        />
                        <SharedWithComponent update={this.handleSharedWithUpdate}
                                            sharedWith={this.state.inputData.shared_with}
                                            error={this.state.sharedWithError}/>

                        <OptionsSelector label={"Permissions"}
                                         options={PermissionOptions()}
                                         selected={this.state.inputData.access_permissions}
                                         update={this.handlePermissionsUpdate}/>

                        <OptionsSelector label={"Status"}
                                         options={StateOptions()}
                                         selected={this.state.inputData.status}
                                         update={this.handleStatusUpdate}/>

                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleClose} color="primary">
                            Cancel
                        </Button>
                        <Button onClick={this.handleSaveBtnClick} color="primary">
                            Save
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        );
    }
}

export default NewNoteDialog;