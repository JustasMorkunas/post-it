import React from 'react';
import {Grid} from '@material-ui/core';
import ProjectElement from "./ProjectElement";
import NoteElement from "./NoteElement";
import ObjHelper from '../Helpers/ObjectEq';

class MainContent extends React.Component{

    constructor(props){
        super(props);
        this.state = this.props.getProjectsState()
    }

    componentDidMount() {
        this.getProjects();
    }

    componentDidUpdate() {
        if (!ObjHelper(this.state, this.props.getProjectsState()))
            this.setState(this.props.getProjectsState())
    }

    getProjects = () => {
        this.props.getProjects().then(() => {
            this.setState(this.props.getProjectsState())
        });

    };

    openProject = (id, e) => {
        this.props.openProject(id,e).then(() => {
            this.setState(this.props.getProjectsState())
        });
    };

    getProjectId = () => {
        return this.props.getProjectId();
    };

    render() {
        return(
            <div>
                <Grid className="projects-container" container spacing={16}>
                    {this.state.projects.map((value)=>{
                        return(
                            <ProjectElement openProject={this.openProject}
                                            projectInfo={value}
                                            updateContents={this.getProjects}
                                            changeLoggedState={this.props.changeLoggedState}
                                            getProjectId={this.getProjectId}
                            />
                        );
                    })}
                </Grid>
                <Grid className="note-container" container spacing={16} alignItems={"stretch"}>
                    {this.state.notes.map((value)=> {
                            return (
                                <NoteElement noteInfo={value}
                                             refreshContent={this.getProjects}
                                             noteId={value.id}
                                             getProjectId={this.getProjectId}
                                />
                            );
                        })}
                </Grid>
            </div>
        );
    }
}

export default MainContent;