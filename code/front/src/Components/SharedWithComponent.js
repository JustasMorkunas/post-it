import React from 'react';
import {TextField, IconButton} from '@material-ui/core';
import {Delete, Add} from '@material-ui/icons';

class SharedWithComponent extends React.Component {

    constructor(props) {
        super(props);
        let defaultState = {
            sharedWith : [],
            currentEntry: '',
            errorOccured: false,
        };

        if(this.props.hasOwnProperty('sharedWith')){
            defaultState.sharedWith = this.props.sharedWith;
        }

        this.state = defaultState;
    };

    componentDidUpdate(newProps) {
        if (this.state.errorOccured != newProps.error) {
            this.state.errorOccured = newProps.error;
            this.setState(this.state);
        }
    }

    handlePropAction = () => {
        if (this.props.hasOwnProperty('update')) {
            this.props.update(this.state.sharedWith);
        }
    };

    handleRemoveAction = (index) => {
        if (index > -1) {
            this.state.sharedWith.splice(index, 1);
            this.setState(this.state);
            this.handlePropAction();
        }
    };

    handleAddAction = () => {
        if (this.state.currentEntry != '') {
            this.state.sharedWith.push(this.state.currentEntry);
            this.state.currentEntry = '';
            this.setState(this.state);
            this.handlePropAction();
        }
    };

    handleChangeAction = (event) => {
        this.state.currentEntry = event.target.value;
        this.setState(this.state);
    }

    getSharedWithItems = (items) => {
        return items.map((item) => {
            return <div className="item">
                <span>{item}</span>
                <IconButton className="remove-btn" onClick={this.handleRemoveAction.bind(this, this.state.sharedWith.indexOf(item))}>
                    <Delete fontSize={"small"}/>
                </IconButton>
            </div>
        });
    };

    render(){
        let error = function () {
          if (this.state.errorOccured) {
              return <span className="shared-with-error">Users with these emails does not exist</span>;
          }
          return null;
        };
        return(
            <div className="shared-with-container">
                <div className="entry">
                    <TextField label="Share with" className="input" onChange={this.handleChangeAction} value={this.state.currentEntry}/>
                    <IconButton className="add-btn" onClick={this.handleAddAction}><Add fontSize={"small"}/></IconButton>
                </div>
                {error}
                <div className="items">
                    {this.getSharedWithItems.bind(this, this.state.sharedWith)()}
                </div>
            </div>
        );
    };

}

export default SharedWithComponent;