import React from 'react';
import {withStyles, Grid, TextField, Button, FormHelperText} from '@material-ui/core';
import { Face, Fingerprint } from '@material-ui/icons'
import Axios from "axios";
const styles = theme => ({
    margin: {
        margin: theme.spacing.unit * 2,
    },
    padding: {
        padding: theme.spacing.unit
    }
});

const logoinAxios = Axios.create({
    baseURL:'http://localhost:8000/'
});

class LoginTab extends React.Component {

    constructor(props){
        super(props);
        this.state = {
          inputs:{
              email: '',
              password: '',
          },
          errors:{
              email:{status: false, message:''},
              password: {status: false, message: ''}
          }
        };
    }

    loginClickHandle = () => {
        logoinAxios.post('/login', this.state.inputs)
            .then((response) => {
                localStorage.setItem('token', response.data.token);
                window.location.reload();
            })
            .catch((error) => {
                this.setErrors(error.response.data);
            });

    };

    setErrors = (errors) => {
        let tempState = this.state;
        console.log(tempState);
        for(var prop in errors){
            console.log(prop);
            if(tempState.errors.hasOwnProperty(prop)){
                tempState.errors[prop].status = true;
                tempState.errors[prop].message = errors[prop][0];
            }
        }
        this.setState(tempState);
    };

    inputChangeHandle = (inputName, newValue) => {
      let tempState = this.state;
      if(tempState.inputs.hasOwnProperty(inputName)){
          tempState.inputs[inputName] = newValue;
          tempState.errors[inputName].status = false;
          tempState.errors[inputName].message = '';
      }
        this.setState(tempState);
    };

    render() {
        const { classes } = this.props;
        return (

                    <div className={classes.margin}>
                        <Grid container spacing={8} alignItems="flex-end">
                            <Grid item>
                                <Face />
                            </Grid>
                            <Grid item md={true} sm={true} xs={true}>
                                <TextField error={this.state.errors.email.status} id="email" label="Email" type="email" onChange={(e) => {this.inputChangeHandle('email', e.target.value)}} fullWidth autoFocus required />
                            </Grid>
                        </Grid>
                        <FormHelperText error={true}>{this.state.errors.email.message}</FormHelperText>
                        <Grid container spacing={8} alignItems="flex-end">
                            <Grid item>
                                <Fingerprint />
                            </Grid>
                            <Grid item md={true} sm={true} xs={true}>
                                <TextField error={this.state.errors.password.status} id="password" label="Password" type="password" onChange={(e) => {this.inputChangeHandle('password', e.target.value)}} fullWidth required />
                            </Grid>
                        </Grid>
                        <FormHelperText error={true}>{this.state.errors.password.message}</FormHelperText>
                        <Grid container justify="center" style={{ marginTop: '10px' }}>
                            <Button variant="outlined" onClick={this.loginClickHandle} color="primary" style={{ textTransform: "none" }}>Login</Button>
                        </Grid>
                    </div>

        );
    }
}

export default withStyles(styles)(LoginTab);