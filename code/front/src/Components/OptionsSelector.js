import React from 'react';
import {Select, MenuItem, InputLabel} from '@material-ui/core';


class OptionsSelector extends React.Component {

    constructor(props) {
        super(props);
        let stateDefaults = {
            open: false,
            options: this.props.options,
            selected: this.props.options[0].value,
        }

        if(this.props.hasOwnProperty('selected')){
            stateDefaults.selected = this.props.selected;
        }
        this.state = stateDefaults;
    };

    handleChange = (event) => {
        this.state.selected = event.target.value;
        this.props.update(event.target.value);
        this.setState(this.state);
    };

    render(){

        let options = this.state.options.map((option) => {
            return <MenuItem selected={false} value={option.value}>{option.label}</MenuItem>
        });

        return(
            <div className="state-selector">
                <InputLabel className="input-label">{this.props.label}</InputLabel>
                <Select className="selector" onChange={this.handleChange} value={this.state.selected}>
                    {options}
                </Select>
            </div>
        );
    };
}

export default OptionsSelector;