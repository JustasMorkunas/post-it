import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Actions from '../Helpers/RequestActions';
import Axios from "axios";

const axiosConfig ={
    baseURL : 'http://localhost:8000',
    headers:{ Authorization: localStorage.getItem('token')},
};

class NewProjectDialog extends React.Component{

    constructor(props) {
        super(props);
        let defaultState = {
            open: false,
            isEdit: false,
            inputData: {
                title: '',
                access_permissions: '777',
                parent_id: 0,
            },
            errors: {
                title: {status: false, message: ''}
            }
        };

        if(this.props.hasOwnProperty('projectData')){
            defaultState.isEdit = true;
            defaultState.inputData.title = this.props.projectData.title;
            defaultState.inputData.parent_id = this.props.projectId();
        }

        this.state = defaultState;
    }

    handleOpen = () => {
        let tempState = this.state;
        tempState.open = true;
        this.setState(tempState);
    };

    handleClose = () => {
        let tempState = this.state;
        tempState.open = false;
        this.setState(tempState);
    };

    handleInputChange = (inputName, value) => {
        let tempState = this.state;
        if (tempState.inputData.hasOwnProperty(inputName)) {
            tempState.inputData[inputName] = value;
            tempState.errors[inputName].state = false;
            tempState.errors[inputName].message = '';
        }
        this.setState(tempState);
    };

    handleInputErrors = (errors) => {
        let tempState = this.state;
        for (var prop in errors) {
            if (tempState.errors.hasOwnProperty(prop)) {
                tempState.errors[prop].status = true;
                tempState.errors[prop].message = errors[prop][0];
            }
        }
        this.setState(tempState);
    };

    handleProjectSave = () => {
        let tempState = this.state;
        tempState.inputData.parent_id = this.props.projectId();
        this.setState(tempState);
        Axios.post('/projects', this.state.inputData, axiosConfig)
            .then((response) => {
                this.handleClose();
                this.props.refreshContent();
            })
            .catch((errors) => {
                this.handleProjectSaveErrors(errors)
            });
    };

    handleProjectSaveErrors = (errors) => {
        switch (errors.response.status) {
            case 401:
                this.handleClose();
                this.props.changeLoggedState();
                break;
            case 400:
                this.handleInputErrors(errors.response.data);
                break;
        }
    };

    handleProjectEdit = () => {
        let tempState = this.state;
        Actions.editProject(this.props.projectData.id, this.state.inputData)
            .then((response) => {
                if (response === 'OK') {
                    this.handleClose();
                    this.props.refreshContent();
                } else {
                    this.handleProjectSaveErrors(response)
                }
            });
    };

    saveButtonHandler = () => {
        if(this.state.isEdit)
            this.handleProjectEdit();
        else
            this.handleProjectSave();
    }

    render(){
        return (
                <div className="menu-item">
                    <div onClick={this.handleOpen} className="menu-btn-wrapper">
                        {this.props.buttonElement}
                    </div>
                <Dialog open={this.state.open} onClose={this.handleClose} aria-labelledby="form-dialog-title">
                    <DialogTitle id="form-dialog-title">Create new project</DialogTitle>
                    <DialogContent >
                        <TextField
                            autoFocus
                            margin="dense"
                            id="title"
                            label="Project title"
                            type="text"
                            fullWidth
                            error={this.state.errors.title.status}
                            value={this.state.inputData.title}
                            onChange={(e) => {this.handleInputChange('title', e.target.value)}}
                        />
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleClose} color="primary">
                            Cancel
                        </Button>
                        <Button onClick={this.saveButtonHandler} color="primary">
                            Save
                        </Button>
                    </DialogActions>
                </Dialog>
                </div>
        );
    }
}

export default NewProjectDialog;