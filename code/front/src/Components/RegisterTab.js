import React from 'react';
import Axios from 'axios';
import {withStyles, Grid, TextField, Button, FormHelperText} from '@material-ui/core';
import { Face, Fingerprint, Spellcheck, Email, Contacts } from '@material-ui/icons'

const styles = theme => ({
    margin: {
        margin: theme.spacing.unit * 2,
    },
    padding: {
        padding: theme.spacing.unit
    }
});

const registerAxios = Axios.create({
    baseURL:'http://localhost:8000/'
});

class RegisterTab extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            inputs:{
                name: '',
                lastname: '',
                email: '',
                password: '',
                password_confirm: '',
            },
            errors: {
                name: {status: false, message: ''},
                lastname: {status: false, message: ''},
                email: {status: false, message: ''},
                password: {status: false, message: ''},
                password_confirm: {status: false, message: ''},
            }
        }
    }

    registerClick = () => {
        registerAxios.post('/register', this.state.inputs)
            .then((response) => {
                localStorage.setItem('token', response.data.token);
                //this.props.changeLoggedState();
                window.location.reload();
            }).catch((error) => {
                var stateTemp = this.state;
                for(var property in error.response.data){
                    stateTemp.errors[property].status = true;
                    stateTemp.errors[property].message = error.response.data[property][0];
                }
                this.setState(stateTemp);
            });
    };

    handleInputChange = (inputName, value) => {
        var tempState = this.state;
        tempState.errors[inputName].status = false;
        tempState.errors[inputName].message = '';
        tempState.inputs[inputName] = value;
        this.setState(tempState);
    };

    render() {
        const { classes } = this.props;
        return (
                <div className={classes.margin}>
                    <Grid container spacing={8} alignItems="flex-end">
                        <Grid item>
                            <Face />
                        </Grid>
                        <Grid item md={true} sm={true} xs={true}>
                            <TextField error={this.state.errors.name.status} id="name" label="Name" type="text" onChange={(e) =>{this.handleInputChange('name', e.target.value)}} fullWidth autoFocus required />
                        </Grid>
                    </Grid>
                    <FormHelperText error={true}>{this.state.errors.name.message}</FormHelperText>
                    <Grid container spacing={8} alignItems="flex-end">
                        <Grid item>
                            <Contacts/>
                        </Grid>
                        <Grid item md={true} sm={true} xs={true}>
                            <TextField error={this.state.errors.lastname.status} id="lastname" label="Lastname" onChange={(e) =>{this.handleInputChange('lastname', e.target.value)}} type="text" fullWidth autoFocus required />
                        </Grid>
                    </Grid>
                    <FormHelperText error={true}>{this.state.errors.lastname.message}</FormHelperText>
                    <Grid container spacing={8} alignItems="flex-end">
                        <Grid item>
                            <Email/>
                        </Grid>
                        <Grid item md={true} sm={true} xs={true}>
                            <TextField error={this.state.errors.email.status} id="email" label="Email" type="email" onChange={(e) =>{this.handleInputChange('email', e.target.value)}} fullWidth autoFocus required />
                        </Grid>
                    </Grid>
                    <FormHelperText error={true}>{this.state.errors.email.message}</FormHelperText>
                    <Grid container spacing={8} alignItems="flex-end">
                        <Grid item>
                            <Fingerprint />
                        </Grid>
                        <Grid item md={true} sm={true} xs={true}>
                            <TextField error={this.state.errors.password.status} id="password" label="Password" type="password" onChange={(e) =>{this.handleInputChange('password', e.target.value)}} fullWidth required />
                        </Grid>
                    </Grid>
                    <FormHelperText error={true}>{this.state.errors.password.message}</FormHelperText>
                    <Grid container spacing={8} alignItems="flex-end">
                        <Grid item>
                            <Spellcheck/>
                        </Grid>
                        <Grid item md={true} sm={true} xs={true}>
                            <TextField error={this.state.errors.password_confirm.status} id="password_confirm" label="Confirm password" type="password" onChange={(e) =>{this.handleInputChange('password_confirm', e.target.value)}} fullWidth required />
                        </Grid>
                    </Grid>
                    <FormHelperText error={true}>{this.state.errors.password_confirm.message}</FormHelperText>
                    <Grid container justify="center" style={{ marginTop: '10px' }}>
                        <Button variant="outlined"
                                color="primary"
                                style={{ textTransform: "none" }}
                                onClick={this.registerClick}>Register</Button>
                    </Grid>
                </div>
        );
    }
}

export default withStyles(styles)(RegisterTab);