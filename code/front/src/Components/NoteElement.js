import React from 'react';
import {
    Card,
    CardContent,
    CardHeader,
    CardActions,
    Grid,
    Typography,
    IconButton,
    withStyles} from "@material-ui/core";
import { Edit, Delete } from '@material-ui/icons'
import NewNoteDialog from '../Components/NewNoteDialog';
import Axios from "axios";

const styles = theme => ({
    root: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.paper,
    },
});

const resourceAxios = Axios.create({
    baseURL : 'http://localhost:8000',
    headers:{ Authorization: localStorage.getItem('token')},
});

class NoteElement extends React.Component{

    constructor(prop){
        super(prop);
        this.state = {
            note: this.props.noteInfo,
        };
    }

    componentDidUpdate(){
        if(this.props.noteInfo.id !== this.state.note.id ||
            this.props.noteInfo.title !== this.state.note.title ||
            this.props.noteInfo.content !== this.state.note.content ||
            this.props.noteInfo.status !== this.state.note.status){
            let tempState = this.state;
            tempState.note = this.props.noteInfo;
            this.setState(tempState);
        }
    }

    deleteNote = () => {
        console.log(this.state);
        resourceAxios.post('/notes/'+this.props.noteId, {'_method':'DELETE'})
            .then((response) => {
                this.props.refreshContent();
            })
            .catch((error) => {
                console.log('Delete error occurred');
                console.log(error.response);
            });
    };

    render(){
        const { classes } = this.props;

            return(
            <Grid className="note-item" item xs={4}>
                <Card raised={true} className={classes.card} style={{backgroundColor:'#ffeb3b'}} >
                    <CardHeader
                        className="note-status"
                        title={this.state.note.title}
                        subheader={this.state.note.status}
                    />
                    <CardContent>
                        <Typography component="p" className="note-content">
                            {this.state.note.content}
                        </Typography>
                    </CardContent>
                    <CardActions>
                        <div style={{flex: 1}}/>
                        <NewNoteDialog projectId={this.props.getProjectId}
                                       changeLoggedState={this.props.changeLoggedState}
                                       refreshContent={this.props.refreshContent}
                                       stateData={this.state.note}
                                       noteId={this.state.note.id}
                                       buttonElement={<IconButton ><Edit fontSize={"small"} /></IconButton>}
                        />

                        <IconButton>
                            <Delete fontSize={"small"}
                                    onClick={this.deleteNote}
                            />
                        </IconButton>
                    </CardActions>
                </Card>
            </Grid>
        );

    }
}

export default withStyles(styles)(NoteElement);