import React from 'react';
import {AppBar, Typography, Toolbar, Fab, Grid, IconButton, withStyles} from '@material-ui/core';
import NewProjectDialog from '../Components/NewProjectDialog';
import NewNoteDialog from '../Components/NewNoteDialog';
import MainContent from '../Components/MainContent';
import ProfileData from '../Components/ProfileData';
import { ArrowBackIos, ExitToApp, CreateNewFolder,  NoteAdd, Menu} from '@material-ui/icons'
import Axios from 'axios';

const styles = theme => ({
    root: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.paper,
    },
});

const resourceAxios = Axios.create({
    baseURL : 'http://localhost:8000',
    headers:{ Authorization: localStorage.getItem('token')},
});

class LoggedPanel extends React.Component {

    constructor(props){
        super(props);

        this.state = {
            projectId : 0,
            previousId: 0,
            projectTitle: 'Home',
            projects: [],
            notes: [],
            mMenuOpen : false,
        }
    }

    openProject = (id, e) => {
        this.state.previousId = this.state.projectId;
        this.state.projectId = id;
        return this.getProjects();
    };

    getProjects = () => {
        return resourceAxios.get('/projects/'+this.state.projectId)
            .then((response) => {
                let tempState = this.state;
                tempState.projectId = response.data.project.id;
                tempState.projectTitle = response.data.project.title;
                tempState.projects = response.data.projects;
                tempState.notes = response.data.notes;
                if(response.data.project.parent_id != null)
                    tempState.previousId = response.data.project.parent_id;
                else
                    tempState.previousId = 0;

                if (this.state.previousId == 0) {
                    tempState.projects.unshift({
                        id: 'shared',
                        title: 'Shared notes'
                    });
                }


                this.setState(tempState);
            }).catch((error) => {
            console.log(error.response);
        });
    };

    getProjectsState = () => {
        return this.state;
    };

    handleLogout = () => {
        localStorage.removeItem('token');
        window.location.reload();
    };

    handleHamburgerToggle = () => {
        this.state.mMenuOpen = !this.state.mMenuOpen;
        this.setState(this.state);
    }

    getProjectId = () => {
        return this.state.projectId;
    };

    render() {
        const { classes } = this.props;

        return (
            <Grid>
                <AppBar>
                    <Toolbar className="menu">
                        <ArrowBackIos className="back-arrow-ic" onClick={this.openProject.bind(this, this.state.previousId)} style={{cursor:'pointer'}}/> <Typography style={{ flex: 1, textAlign:'left' }} variant="h6" color="inherit" className="menu-dir-txt">{this.state.projectTitle}</Typography>

                        <div className={this.state.mMenuOpen ? "menu-options open" : "menu-options"}>
                            <IconButton className={this.state.mMenuOpen ? "hamburger open" : "hamburger"} onClick={this.handleHamburgerToggle}>
                                <Menu/>
                            </IconButton>
                            <div className="menu-btn-container">
                                <NewProjectDialog projectId={this.getProjectId}
                                                  changeLoggedState={this.props.changeLoggedState}
                                                  refreshContent={this.getProjects}
                                                  buttonElement={<Fab variant="extended" color="secondary" className="menu-btn pink"><CreateNewFolder className="menu-btn-ic"/><span className="menu-btn-txt">Create project</span></Fab>}
                                />
                                <NewNoteDialog projectId={this.getProjectId}
                                               changeLoggedState={this.props.changeLoggedState}
                                               refreshContent={this.getProjects}
                                               buttonElement={<Fab variant="extended" color="secondary" className="menu-btn pink" ><NoteAdd className="menu-btn-ic"/><span className="menu-btn-txt">Create post-it</span></Fab>}
                                />
                                <ProfileData/>
                                <IconButton style={{color:"white"}} className="logout-bnt" onClick={this.handleLogout}>
                                    <ExitToApp className="logout-ic"/>
                                    <span className="menu-btn-txt mobile-only logout-txt">Log out</span>
                                </IconButton>
                            </div>
                        </div>

                    </Toolbar>
                </AppBar>
                <MainContent changeLoggedState={this.props.changeLoggedState}
                             openProject={this.openProject}
                             getProjects={this.getProjects}
                             getProjectsState={this.getProjectsState}
                             getProjectId={this.getProjectId}
                />
            </Grid>
        );
    }
}

export default withStyles(styles)(LoggedPanel);