import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import LoginTab from '../Components/LoginTab';
import RegisterTab from '../Components/RegisterTab';
import {Grid, Paper, Toolbar} from '@material-ui/core';
import LogoImg from '../logo.png';

function TabContainer(props) {
    return (
        <Typography component="div" style={{ padding: 8 * 3 }}>
            {props.children}
        </Typography>
    );
}

TabContainer.propTypes = {
    children: PropTypes.node.isRequired,
};

const styles = theme => ({
    root: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.paper,
    },
});

class AuthPanel extends React.Component {

    state = {
        value: 0,
    };

    handleChange = (event, value) => {
        this.setState({ value });
    };

    render() {
        const { classes } = this.props;
        const { value } = this.state;

        return (
            <div>
                <AppBar>
                <Toolbar className="menu">
                    <img className="logo-img" src={LogoImg} alt="eco-light"/>
                    <Typography style={{ flex: 1, textAlign:'left' }} variant="h6" color="inherit" className="menu-dir-txt">Post-it</Typography>
                </Toolbar>
                </AppBar>
                <Paper>
                    <div className={classes.root}>
                        <AppBar position="static">
                            <Tabs value={value} onChange={this.handleChange}>
                                <Tab label="Login" />
                                <Tab label="Register" />
                            </Tabs>
                        </AppBar>
                        {value === 0 && <TabContainer><LoginTab changeLoggedState = {this.props.changeLoggedState}/></TabContainer>}
                        {value === 1 && <TabContainer><RegisterTab changeLoggedState = {this.props.changeLoggedState}/></TabContainer>}
                    </div>
                </Paper>
            </div>
        );
    }
}

AuthPanel.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(AuthPanel);