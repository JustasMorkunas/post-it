<?php
/**
 * Created by PhpStorm.
 * User: Justas
 * Date: 2019-12-16
 * Time: 21:08
 */
declare(strict_types=1);


namespace analysisRules;

use PhpParser\Node;
use PHPStan\Analyser\Scope;
use PHPStan\Rules\Rule;

class ConstantRule implements Rule
{

    /**
     * @return string Class implementing \PhpParser\Node
     */
    public function getNodeType(): string
    {
        return Node\Stmt\ClassConst::class;
    }

    /**
     * @param \PhpParser\Node $node
     * @param \PHPStan\Analyser\Scope $scope
     * @return (string|RuleError)[] errors
     */
    public function processNode(Node $node, Scope $scope): array
    {
        if (($node->flags & Node\Stmt\Class_::VISIBILITY_MODIFIER_MASK) === 0) {
            return [
                sprintf(
                    'Constant %s must declare a visibility keyword.',
                    $node->consts[0]->name->name
                )
            ];
        }
        return [];
    }
}
