<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;

$factory->define(App\Project::class, function (Faker $faker) {
    return [
        'parent_id' => null,
        'title' => $faker->word,
        'about' => $faker->text,
        'access_permissions' => '7',
    ];
});
