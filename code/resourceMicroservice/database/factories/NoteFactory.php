<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;

$factory->define(App\Note::class, function (Faker $faker) {
    return [
        'project_id' => $faker->randomNumber(),
        'owner_id' => $faker->randomNumber(),
        'status' => str_random(5),
        'title' => str_random(5),
        'content' => $faker->text,
        'deadline' => $faker->dateTime(),
        'last_modified_by' => $faker->randomNumber(),
    ];
});
