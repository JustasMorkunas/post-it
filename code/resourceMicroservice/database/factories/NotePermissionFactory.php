<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;

$factory->define(App\NotePermission::class, function (Faker $faker) {
    return [
        'note_id' => $faker->randomNumber(),
        'user_id' => $faker->randomNumber(),
        'permission' => $faker->randomNumber(),
    ];
});
