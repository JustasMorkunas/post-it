<?php

use \App\Http\Handlers\ProjectsHandlers\CreateProjectHandler;
use \App\Http\Handlers\NotesHandlers\CreateNoteHandler;
use \App\Http\Handlers\ProjectsHandlers\GetAllProjectsHandler;
use \App\Http\Handlers\ProjectsHandlers\GetProjectHandler;
use \App\Http\Handlers\NotesHandlers\GetNoteHandler;
use \App\Http\Handlers\NotesHandlers\DeleteNoteHandler;
use \App\Http\Handlers\NotesHandlers\UpdateNoteHandler;
use \App\Http\Handlers\ProjectsHandlers\DeleteProjectHandler;
use \App\Http\Handlers\ProjectsHandlers\UpdateProjectHandler;
use App\Http\Handlers\UserHandlers\editRequestHandler;
use App\Http\Handlers\UserHandlers\getRequestHandler;
use App\Http\Handlers\UserHandlers\loginRequestHandler;
use App\Http\Handlers\UserHandlers\registerRequestHandler;

Route::get('/', function () {
    return view('welcome');
});
Route::group(['middleware' => ['cors', 'jwt.verify' ]], function(){

    Route::get('/projects', GetAllProjectsHandler::class)->name('get-all-projects');
    Route::get('/projects/shared', function (\App\Http\Requests\NoteRequests\GetNoteRequest $request) {
        return (new GetNoteHandler())->getSharedNotes($request);
    })->name('get-shared-notes');
    Route::get('/projects/{id}', GetProjectHandler::class)->name('get-project');
    Route::post('/projects', CreateProjectHandler::class)->name('create-project');
    Route::delete('/projects/{id}', DeleteProjectHandler::class)->name('delete-project');
    Route::put('/projects/{id}', UpdateProjectHandler::class)->name('update-project');
    Route::get('/projects/{id}/notes', function ($id) {
        return (new GetProjectHandler())->getNotes($id);
    });

    Route::get('/projects/{projectId}/notes/{which}', function ($projectId, $which) {
        return (new GetProjectHandler())->getNote($projectId, $which);
    } );

    Route::post('/notes', CreateNoteHandler::class)->name('create-note');



    Route::get('/notes/{id}', GetNoteHandler::class)->name('get-note');

    Route::delete('/notes/{id}', DeleteNoteHandler::class)->name('delete-note');
    Route::put('/notes/{id}', UpdateNoteHandler::class)->name('update-note');

    Route::get('/user', getRequestHandler::class)->name('get-user');
    Route::post('/user/{id}', editRequesthandler::class)->name('update-profile');
    Route::delete('/user/{id}', function (){
        return response()->json(['status' => true,'message' => 'User sucesfully deleted'], \Illuminate\Http\JsonResponse::HTTP_OK);
    });

    Route::get('/validate/user', function (){
        return response()->json(['status' => true], \Illuminate\Http\JsonResponse::HTTP_OK);
    });
});

Route::group(['middleware'=> ['cors']], function(){
    Route::post('/register', registerRequestHandler::class)->name('register');
    Route::post('/login', loginRequestHandler::class)->name('login');
});

