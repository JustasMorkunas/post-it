<?php

namespace App;

use App\DTO\UserDTO;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;

/**
 * @method static create(array $array)
 * @method static where(string $string, string $getEmail)
 * @method static find($id)
 * @property int id
 * @property string name
 * @property string lastname
 * @property string email
 */
class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'lastname', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    /**
     * @param UserDTO $userDTO
     */
    public function updateUserProperties(UserDTO $userDTO) : void
    {
        $this->name = $userDTO->getFirstname();
        $this->lastname = $userDTO->getLastname();
        $this->email = $userDTO->getEmail();

        $this->update();
    }
}
