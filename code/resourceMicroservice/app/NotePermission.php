<?php

namespace App;

use App\Http\Requests\NoteRequests\createNoteRequest;
use App\Http\Requests\NoteRequests\EditNoteRequest;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class NotePermission is a modal class for CRUD operations with note access permissions.
 */
class NotePermission extends Model
{
    /**
     * @var String[]
     */
    protected $fillable = [
        'note_id', 'user_id', 'permission'
    ];


    /**
     * @param int $noteId
     * @param createNoteRequest $createNoteRequest
     * @return void
     */
    public static function createPermissions($noteId, createNoteRequest $createNoteRequest)
    {
        $usersData = User::select('id')->whereIn('email', $createNoteRequest->getSharedWith())->get();

        foreach ($usersData as $userData) {
            if ($createNoteRequest->getAccessPermissions() != 0) {
                $permission = new NotePermission();
                $permission->note_id = $noteId;
                $permission->user_id = $userData->id;
                $permission->permission = $createNoteRequest->getAccessPermissions();
                $permission->save();
            }
        }
    }

    /**
     * @param Note $note
     * @param EditNoteRequest $request
     * @return void
     */
    public static function updatePermissions(Note $note, EditNoteRequest $request)
    {
        $usersData = User::select('id')->whereIn('email', $request->getSharedWith())->get();


        foreach ($usersData as $userData) {

            if ($request->getAccessPermissions() === 0) {
                DB::table('note_permissions')
                    ->where('user_id', '=', $userData->id)
                    ->where('note_id', '=', $note->id)
                    ->delete();
            } else {
                NotePermission::updateOrCreate(
                    ['note_id' => $note->id, 'user_id' => $userData->id],
                    ['permission' => $request->getAccessPermissions()]);
            }
        }
    }

    /**
     * @param Note $note
     * @param Request $request
     * @return NotePermission
     */
    public static function getPermissions(Note $note, Request $request)
    {
        $notePermission = NotePermission::where('note_id', '=', $note->id)
            ->where('user_id', '=', $request->attributes->get('user_id'))
            ->get();

        if ($notePermission->isEmpty()) {
            return new NotePermission();
        }

        return $notePermission->first();
    }

    /**
     * @param Note $note
     * @return User[]
     */
    public static function getNoteUsers(Note $note)
    {
        return DB::table('users')->join('note_permissions', 'users.id', 'note_permissions.user_id')
            ->where('note_permissions.note_id', $note->id)
            ->select('users.email')
            ->get()
            ->map(function ($item) {
                return $item->email;
            })
            ->toArray();
    }

    /**
     * @param Note $note
     * @return int
     */
    public static function getNotePermission(Note $note)
    {
        $permission = DB::table('note_permissions')
            ->where('note_id', '=', $note->id)
            ->select('permission')
            ->get();

        if ($permission->isNotEmpty()) {
            return $permission->first()->permission;
        }

        return 0;
    }

    /**
     * @param Request $request
     * @return Collection
     */
    public static function getPermissionsByUser(Request $request)
    {
        $notePermissions = NotePermission::where('user_id', '=', $request->attributes->get('user_id'))->get();

        if ($notePermissions->isEmpty()) {
            return new Collection();
        }

        return $notePermissions;
    }
}
