<?php
/**
 * Created by PhpStorm.
 * User: Justas
 * Date: 2019-05-29
 * Time: 22:47
 */
declare(strict_types=1);

namespace App\DTO;


class UserDTO
{
    /**
     * @var string
     */
    private $firstname;
    /**
     * @var string
     */
    private  $lastname;
    /**
     * @var string
     */
    private $email;
    /**
     * @var string
     */
    private $password;
    /**
     * @return string
     */
    public function getFirstname(): string
    {
        return $this->firstname;
    }

    /**
     * @param string $firstname
     * @return UserDTO
     */
    public function setFirstname(string $firstname): UserDTO
    {
        $this->firstname = $firstname;
        return $this;
    }

    /**
     * @return string
     */
    public function getLastname(): string
    {
        return $this->lastname;
    }

    /**
     * @param string $lastname
     * @return UserDTO
     */
    public function setLastname(string $lastname): UserDTO
    {
        $this->lastname = $lastname;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return UserDTO
     */
    public function setEmail(string $email): UserDTO
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return UserDTO
     */
    public function setPassword(string $password): UserDTO
    {
        $this->password = $password;
        return $this;
    }
}
