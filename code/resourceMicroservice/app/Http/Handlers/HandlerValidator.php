<?php

declare(strict_types=1);


namespace App\Http\Handlers;


use App\Http\Handlers\ModelValidators\ValidatorFactoryMethod;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class HandlerValidator
{

    /**
     * @var Model
     */
    protected $model;

    /**
     * @var Request
     */
    protected $request;

    public function __construct(Model $model, Request $request)
    {
        $this->model = $model;
        $this->request = $request;
    }

    public function validateData() : void
    {
        $this->checkIfModelWasFound()
            ->checkIfUserCanManageModel();

    }

    protected function checkIfModelWasFound() : HandlerValidator
    {
        if($this->model == null)
            throw new HttpResponseException(response()->json([
                'errors' => ['Not found']
            ], JsonResponse::HTTP_NOT_FOUND));

        return $this;
    }

    protected function checkIfUserCanManageModel() : HandlerValidator
    {
        (new ValidatorFactoryMethod($this->model, $this->request))->getValidator()->validateOwner();
        return $this;
    }

}
