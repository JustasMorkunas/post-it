<?php

declare(strict_types=1);


namespace App\Http\Handlers\UserHandlers;


use App\Http\Controllers\Controller;
use App\Http\Handlers\ModelValidators\ValidatorFactoryMethod;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class getRequestHandler extends Controller
{

    public function __invoke(Request $request)
    {
        $user = User::find($request->attributes->get('user_id'));

        (new ValidatorFactoryMethod($user, $request))->getValidator()->validateOwner();

        return response()->json(['user' => $user], JsonResponse::HTTP_OK);
    }

}
