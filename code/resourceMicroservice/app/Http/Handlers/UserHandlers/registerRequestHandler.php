<?php
/**
 * Created by PhpStorm.
 * User: Justas
 * Date: 2018-12-06
 * Time: 13:47
 */
declare(strict_types=1);


namespace App\Http\Handlers\UserHandlers;


use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequests\registerRequest;
use Illuminate\Http\JsonResponse;
use App\User;
use Tymon\JWTAuth\Facades\JWTAuth;

class registerRequestHandler extends Controller
{
    public function __invoke(registerRequest $registerRequest) : JsonResponse
    {
        $user = User::create([
            'name' => $registerRequest->getName(),
            'lastname' => $registerRequest->getLastname(),
            'email' => $registerRequest->getEmail(),
            'password' => $registerRequest->getUserPassword(),
        ]);

        $token = JWTAuth::fromUser($user);

        return response()->json([
            'message' => 'User successfully created',
            'user' => $user,
            'token' => 'Bearer '.$token,
            'actions' => [Route('login')],
        ], JsonResponse::HTTP_OK);
    }
}
