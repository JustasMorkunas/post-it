<?php

declare(strict_types=1);


namespace App\Http\Handlers\UserHandlers;


use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequests\loginRequest;
use App\User;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Tymon\JWTAuth\Facades\JWTAuth;


class loginRequestHandler extends Controller
{
    /**
     * @var loginRequest
     */
    protected $loginRequest;

    protected $token;

    public function __invoke(loginRequest $loginRequest) : JsonResponse
    {
        $this->loginRequest = $loginRequest;
        $user = User::where('email', $this->loginRequest->getEmail())->first();

        $this->token = JWTAuth::customClaims(['user_id' => $user->id])->attempt($this->loginRequest->only('email', 'password'));

        $this->throwExceptionIfTokenDoesntExists();

        return response()->json([
            'user' => $user,
            'token' => 'Bearer '.$this->token,
        ], JsonResponse::HTTP_OK);

    }

    protected function throwExceptionIfTokenDoesntExists() : void
    {
        if($this->token === false)
            throw new HttpResponseException(response()->json([
                'error'=>['password' => ['Incorrect password.']],
            ], JsonResponse::HTTP_UNAUTHORIZED));
    }


}
