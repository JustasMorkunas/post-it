<?php
/**
 * Created by PhpStorm.
 * User: Justas
 * Date: 2019-05-30
 * Time: 18:30
 */
declare(strict_types=1);


namespace App\Http\Handlers\UserHandlers;


use App\Http\Controllers\Controller;
use App\Http\Handlers\HandlerValidator;
use App\Http\Requests\UserRequests\editRequest;
use App\User;
use Symfony\Component\HttpFoundation\JsonResponse;

class editRequestHandler extends Controller
{
    public function __invoke($id, editRequest $request) : JsonResponse
    {
        $user = User::find($id);

        (new HandlerValidator($user, $request))->validateData();

        $user->updateUserProperties($request->getUserDTO());

        return response()->json(['user' => $user], JsonResponse::HTTP_OK);
    }

}
