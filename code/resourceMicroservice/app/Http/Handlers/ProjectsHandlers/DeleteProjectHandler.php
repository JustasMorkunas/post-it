<?php
/**
 * Created by PhpStorm.
 * User: Justas
 * Date: 2019-01-05
 * Time: 17:04
 */
declare(strict_types=1);


namespace App\Http\Handlers\ProjectsHandlers;


use App\Http\Controllers\Controller;
use App\Http\Handlers\HandlerValidator;
use App\Project;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class DeleteProjectHandler extends Controller
{
    /**
     * @var Request
     */
    protected $request;

    /**
     * @var Project
     */
    protected $project;

    public function __invoke($id, Request $request)
    {
        $this->request = $request;
        $this->project = Project::find($id);

        (new HandlerValidator($this->project, $this->request))->validateData();

        $this->project->delete();

        return response()->json([
            'message' => 'Project successfully deleted',
            'actions' => [
                'create' => Route('create-project'),
            ]
        ], JsonResponse::HTTP_OK);
    }

}
