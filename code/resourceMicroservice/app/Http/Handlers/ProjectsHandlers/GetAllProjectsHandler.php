<?php
/**
 * Created by PhpStorm.
 * User: Justas
 * Date: 2019-01-02
 * Time: 16:02
 */
declare(strict_types=1);


namespace App\Http\Handlers\ProjectsHandlers;


use App\Http\Controllers\Controller;
use App\Project;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class GetAllProjectsHandler extends Controller
{

    public function __invoke(Request $request) : JsonResponse
    {
        $projects = Project::where('owner_id', $request->attributes->get('user_id'))
            ->get(['id', 'title']);


        return response()->json(['projects' => $projects], JsonResponse::HTTP_OK);
    }

}
