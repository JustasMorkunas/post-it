<?php
/**
 * Created by PhpStorm.
 * User: Justas
 * Date: 2019-01-02
 * Time: 16:15
 */
declare(strict_types=1);


namespace App\Http\Handlers\ProjectsHandlers;


use App\Http\Controllers\Controller;
use App\Http\Handlers\HandlerValidator;
use App\Note;
use App\Project;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class GetProjectHandler extends Controller
{
    /**
     * @var Request
     */
    protected $request;

    /**
     * @var Project
     */
    protected $project;

    /**
     * @var Project
     */
    protected $projects;

    /**
     * @var Note
     */
    protected $notes;

    public function __invoke($id, Request $request) : JsonResponse
    {
        $this->request = $request;
        if ($id != 0) {
            $this->project = Project::find($id);
            $this->checkIfExists();
            $this->getProjectName();
        } else {
            $this->project = Project::where('owner_id', $request->attributes->get('user_id'))->where('parent_id', '=', null)->first();
            if (!$this->project) {
                $this->project = new Project;
                $this->project->setDefaults($this->request);
                $this->project->save();
            }
        }

        $this->checkIfExists();

        (new HandlerValidator($this->project, $this->request))->validateData();
        $this->setAssociatedResources();

        return response()->json([
            'project' => $this->project,
            'projects' => $this->projects,
            'notes' => $this->notes,
            'actions' => [
                'project' => ['create' => Route('create-project')],
                'notes' => ['create' => Route('create-note')],
            ],
        ], JsonResponse::HTTP_OK);
    }

    public function getNotes($projectId)
    {
        if ($projectId == 0) {
            $projectId = null;
        }
        $notes = Note::where('project_id', $projectId)->get(['id', 'title', 'content', 'status']);
        if ($notes->isEmpty()) {
            throw new HttpResponseException(response()->json([
                'errors' => ['Project not found']
            ], JsonResponse::HTTP_NOT_FOUND));
        } else {
            return response()->json([
                'notes' => $notes
            ], JsonResponse::HTTP_OK);
        }

    }

    public function getNote($projectId, $which)
    {
        if ($projectId == 0) {
            $projectId = null;
        }
        $notes = Note::where('project_id', $projectId)->get(['id', 'title', 'content', 'status']);
        if ($notes->isEmpty()) {
            throw new HttpResponseException(response()->json([
                'errors' => ['Project not found']
            ], JsonResponse::HTTP_NOT_FOUND));
        } else {
            return response()->json([
                'note' => $notes->toArray()[$which]
            ], JsonResponse::HTTP_OK);
        }
    }

    private function checkIfExists()
    {
        if ($this->project == null) {
            throw new HttpResponseException(response()->json([
                'errors' => ['Project not found'],
            ], JsonResponse::HTTP_NOT_FOUND));
        }
    }

    protected function getProjectName()
    {
        $project = $this->project;
        while ($project->parent_id != null) {
            $project = Project::find($project->parent_id);
            if ($project == null)
                return;

            $this->project->title = $project->title . '/' . $this->project->title;
        }
    }

    protected function setAssociatedResources(): void
    {
        $this->projects = Project::where('parent_id', $this->project->id)->get(['id', 'title']);
        $this->notes = Note::where('project_id', $this->project->id)->get(['id', 'title', 'content', 'status']);
    }

}
