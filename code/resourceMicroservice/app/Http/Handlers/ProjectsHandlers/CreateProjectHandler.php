<?php
/**
 * Created by PhpStorm.
 * User: Justas
 * Date: 2018-12-28
 * Time: 16:58
 */
declare(strict_types=1);


namespace App\Http\Handlers\ProjectsHandlers;

use App\Http\Controllers\Controller;

use App\Http\Requests\ProjectRequests\createProjectRequest;
use App\Project;
use Illuminate\Http\JsonResponse;

class CreateProjectHandler extends Controller
{
    public function __invoke(createProjectRequest $createProjectRequest) : JsonResponse
    {
        $project = new Project;
        $project->setPropertiesFromRequest($createProjectRequest);
        $project->save();

        return response()->json([
            'project' => $project,
            'actions' => [
                'get' => Route('get-project', $project->id),
            ],
        ], JsonResponse::HTTP_OK);
    }
}
