<?php
/**
 * Created by PhpStorm.
 * User: Justas
 * Date: 2019-01-05
 * Time: 17:27
 */
declare(strict_types=1);


namespace App\Http\Handlers\ProjectsHandlers;


use App\Http\Controllers\Controller;
use App\Http\Handlers\HandlerValidator;
use App\Http\Requests\ProjectRequests\EditProjectRequest;
use App\Project;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;

class UpdateProjectHandler extends Controller
{
    /**
     * @var Project
     */
    protected $project;

    /**
     * @var EditProjectRequest
     */
    protected $request;

    public function __invoke($id, EditProjectRequest $editProjectRequest){
        $this->request = $editProjectRequest;
        $this->project = Project::find($id);

        $this->checkIfExists();

        (new HandlerValidator($this->project, $this->request))->validateData();

        $this->project->setPropertiesFromRequest($editProjectRequest);
        $this->project->save();

        return response()->json([
            'project' => $this->project,
            'actions' => [
              'get' => Route('get-project', $this->project->id),
            ],
        ], JsonResponse::HTTP_OK);
    }

    private function checkIfExists() {
        if ($this->project == null)
            throw new HttpResponseException(response()->json([
                'errors' => ['Project not found']
            ], JsonResponse::HTTP_NOT_FOUND));
    }
}
