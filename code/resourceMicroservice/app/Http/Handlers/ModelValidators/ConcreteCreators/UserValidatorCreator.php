<?php
/**
 * Created by PhpStorm.
 * User: Justas
 * Date: 2019-05-30
 * Time: 18:40
 */
declare(strict_types=1);


namespace App\Http\Handlers\ModelValidators\ConcreteCreators;


use App\Http\Handlers\ModelValidators\Creator;
use App\User;
use Illuminate\Http\Exceptions\HttpResponseException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;


class UserValidatorCreator implements Creator
{
    /**
     * @var User
     */
    private $user;

    /**
     * @var Request
     */
    private $request;

    public function __construct(User $user, Request $request)
    {
      $this->user = $user;
      $this->request = $request;
    }

    public function validateOwner(): void
    {
        if ($this->user->id != $this->request->attributes->get('user_id'))
            throw new HttpResponseException(response()->json([
                'errors' => ['You don\'t have permission to access this user data'],
            ], JsonResponse::HTTP_FORBIDDEN));
    }
}
