<?php
/**
 * Created by PhpStorm.
 * User: Justas
 * Date: 2019-01-08
 * Time: 19:03
 */
declare(strict_types=1);


namespace App\Http\Handlers\ModelValidators\ConcreteCreators;


use App\Exceptions\ProcessingException;
use App\Http\Handlers\ModelValidators\Creator;
use App\Http\Requests\NoteRequests\EditNoteRequest;
use App\Note;
use App\NotePermission;
use App\Http\Requests\NoteRequests\DeleteNoteRequest;
use App\Http\Requests\NoteRequests\GetNoteRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class NoteValidatorCreator implements Creator
{
    /**
     * @var Note
     */
    protected $note;

    /**
     * @var Request
     */
    protected $request;

    public function __construct(Note $note, Request $request)
    {
        $this->note = $note;
        $this->request = $request;
    }

    public function validateOwner(): void
    {
        if (!$this->isOwner() && !$this->isSharedWith(NotePermission::getPermissions($this->note, $this->request)))
            throw new HttpResponseException(response()->json([
                'errors' => ['You don\'t have permission to access this note'],
            ], JsonResponse::HTTP_FORBIDDEN));
    }

    public function isOwner(): bool
    {
        return $this->note->owner_id == $this->request->attributes->get('user_id');
    }


    /**
     * @param NotePermission $notePermission
     * @return bool
     * @throws ProcessingException
     */
    public function isSharedWith($notePermission): bool
    {

        if ($notePermission == null) {
            return false;
        }

        if ($notePermission->permission == null) {
            return false;
        }

        $permissionsArray = AccessPermission::getPermissions($notePermission->permission);

        return $this->isActionAllowed($permissionsArray);
    }

    protected function isActionAllowed($permissions): bool
    {
        if ($this->request instanceof EditNoteRequest)
            return in_array(AccessPermission::EDIT, $permissions);

        else if ($this->request instanceof DeleteNoteRequest)
            return in_array(AccessPermission::DELETE, $permissions);

        else if ($this->request instanceof GetNoteRequest)
            return in_array(AccessPermission::VIEW, $permissions);
        else
            throw new ProcessingException("Unknown action for shared user");
    }

    public function getPermissions($permissionCode) : array
    {
        switch ($permissionCode) {
            case 7:
                return [
                    AccessPermission::VIEW,
                    AccessPermission::EDIT,
                    AccessPermission::DELETE
                ];
            case 6:
                return [
                    AccessPermission::VIEW,
                    AccessPermission::EDIT
                ];
            case 5:
                return [
                    AccessPermission::VIEW,
                    AccessPermission::DELETE
                ];
            case 4:
                return [
                    AccessPermission::VIEW
                ];
        }

        throw new ProcessingException("Unknown permission value");
    }

}
