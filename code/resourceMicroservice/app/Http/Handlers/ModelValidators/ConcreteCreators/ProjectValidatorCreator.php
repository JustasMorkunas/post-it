<?php
/**
 * Created by PhpStorm.
 * User: Justas
 * Date: 2019-01-08
 * Time: 21:55
 */
declare(strict_types=1);


namespace App\Http\Handlers\ModelValidators\ConcreteCreators;


use App\Http\Handlers\ModelValidators\Creator;
use App\Project;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

/**
 * Class ProjectValidatorCreator is a concrete validator class which checks if user is authorized to access specified
 * project. If user is not authorized, program execution is stoped.
 */
class ProjectValidatorCreator implements Creator
{
    /**
     * @var Project
     */
    protected $project;

    /**
     * @var Request
     */
    protected $request;

    /**
     * ProjectValidatorCreator constructor.
     * @param Project $project
     * @param Request $request
     */
    public function __construct(Project $project, Request $request)
    {
        $this->project = $project;
        $this->request = $request;
    }

    /**
     * @throws HttpResponseException
     * @return void
     */
    public function validateOwner(): void
    {
        if ($this->project->owner_id !== $this->request->attributes->get('user_id')) {
            throw new HttpResponseException(response()->json([
                'errors' => ['You don\'t have permission to access this project'],
            ], JsonResponse::HTTP_FORBIDDEN));
        }
    }
}
