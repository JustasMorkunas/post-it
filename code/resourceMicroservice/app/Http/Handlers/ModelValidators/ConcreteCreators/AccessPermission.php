<?php
/**
 * Created by PhpStorm.
 * User: Justas
 * Date: 2019-10-29
 * Time: 19:05
 */
declare(strict_types=1);


namespace App\Http\Handlers\ModelValidators\ConcreteCreators;


use UnexpectedValueException;

class AccessPermission
{
    public const VIEW = "VIEW";

    public const EDIT = "EDIT";

    public const DELETE = "DELETE";

    public static function getPermissions(int $permissionCode) : array
    {
        switch ($permissionCode){
            case 7:
                return [
                    AccessPermission::VIEW,
                    AccessPermission::EDIT,
                    AccessPermission::DELETE
                ];
            case 6:
                return [
                    AccessPermission::VIEW,
                    AccessPermission::EDIT
                ];
            case 5:
                return [
                    AccessPermission::VIEW,
                    AccessPermission::DELETE
                ];
            case 4:
                return [
                    AccessPermission::VIEW
                ];
        }

        throw new UnexpectedValueException("Unknown permission value");
    }
}
