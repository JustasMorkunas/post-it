<?php
/**
 * Created by PhpStorm.
 * User: Justas
 * Date: 2019-01-08
 * Time: 19:02
 */
declare(strict_types=1);


namespace App\Http\Handlers\ModelValidators;


interface Creator
{

    public function validateOwner() : void;

}
