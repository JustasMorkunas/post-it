<?php
/**
 * Created by PhpStorm.
 * User: Justas
 * Date: 2019-01-08
 * Time: 19:00
 */
declare(strict_types=1);


namespace App\Http\Handlers\ModelValidators;


use App\Exceptions\ProcessingException;
use App\Http\Handlers\ModelValidators\ConcreteCreators\NoteValidatorCreator;
use App\Http\Handlers\ModelValidators\ConcreteCreators\ProjectValidatorCreator;
use App\Http\Handlers\ModelValidators\ConcreteCreators\UserValidatorCreator;
use App\Note;
use App\Project;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

/**
 * Class ValidatorFactoryMethod is a factory class which creates concrete validator depending on provided model class
 * and request type.
 */
class ValidatorFactoryMethod
{

    /**
     * @var Model
     */
    protected $model;

    /**
     * @var Request
     */
    protected $request;

    public function __construct(Model $model, Request $request)
    {
        $this->model = $model;
        $this->request = $request;
    }

    /**
     * @return Creator concrete validator
     * @throws ProcessingException processing exception is thrown if validator for specified request does not exist.
     */
    public function getValidator() : Creator
    {
        if ($this->model instanceof Note) {
            return new NoteValidatorCreator($this->model, $this->request);
        } else if ($this->model instanceof Project) {
            return new ProjectValidatorCreator($this->model, $this->request);
        } else if ($this->model instanceof User) {
            return new UserValidatorCreator($this->model, $this->request);
        }

        throw new ProcessingException("Validator for model class not found");
    }

}
