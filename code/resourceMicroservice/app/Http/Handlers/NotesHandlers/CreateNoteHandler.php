<?php
/**
 * Created by PhpStorm.
 * User: Justas
 * Date: 2019-01-02
 * Time: 15:39
 */
declare(strict_types=1);


namespace App\Http\Handlers\NotesHandlers;


use App\Http\Controllers\Controller;
use App\Http\Requests\NoteRequests\createNoteRequest;
use App\Note;
use App\NotePermission;
use Illuminate\Http\JsonResponse;
use function Psy\debug;

class CreateNoteHandler extends Controller
{
    public function __invoke(createNoteRequest $request) : JsonResponse
    {
        $note = new Note;
        $note->setPropertiesFromRequest($request);
        $note->save();

        NotePermission::createPermissions($note->id, $request);


        return response()->json([
            'note' => $note,
            'actions' => [
                'get' => Route('get-note', $note->id),
                'delete' => Route('delete-note', $note->id),
                'update' => Route('update-note', $note->id),
            ]
        ], JsonResponse::HTTP_OK);
    }
}
