<?php
/**
 * Created by PhpStorm.
 * User: Justas
 * Date: 2019-01-02
 * Time: 16:41
 */
declare(strict_types=1);


namespace App\Http\Handlers\NotesHandlers;


use App\Http\Controllers\Controller;
use App\Http\Handlers\HandlerValidator;
use App\Http\Requests\NoteRequests\GetNoteRequest;
use App\Note;
use App\NotePermission;
use App\Project;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class GetNoteHandler extends Controller
{
    /**
     * @var Request
     */
    protected $request;

    /**
     * @var Note
     */
    protected $note;

    public function __invoke($id, GetNoteRequest $request): JsonResponse
    {
        $this->request = $request;
        $this->note = Note::find($id);

        $this->checkIfExists();

        (new HandlerValidator($this->note, $this->request))->validateData();
        $noteData = $this->note->toArray();
        $noteData['shared_with'] = NotePermission::getNoteUsers($this->note);
        $noteData['access_permissions'] = NotePermission::getNotePermission($this->note);


        return response()->json([
            'note' => $noteData,
            'actions' => [
                'update' => Route('update-note', $this->note->id),
                'delete' => Route('delete-note', $this->note->id),
            ],
        ], JsonResponse::HTTP_OK);
    }

    private function checkIfExists()
    {
        if ($this->note == null) {
            throw new HttpResponseException(response()->json([
                'errors' => ['Note not found']
            ], JsonResponse::HTTP_NOT_FOUND));
        }
    }

    public function getSharedNotes(GetNoteRequest $request)
    {
        $this->request = $request;

        $notes = Note::getSharedNotes(NotePermission::getPermissionsByUser($request));
        $parentProject = Project::where('owner_id', $request->attributes->get('user_id'))->where('parent_id', '=', null)->first();

        return response()->json([
            'notes' => $notes,
            'project' => [
                'id' => 'shared',
                'owner_id' => $request->attributes->get('user_id'),
                'parent_id' => $parentProject->id,
                'title' => 'Shared notes',
                'about' => '',
            ],
            'projects' => [],
            'actions' => [],
        ], JsonResponse::HTTP_OK);

    }


}
