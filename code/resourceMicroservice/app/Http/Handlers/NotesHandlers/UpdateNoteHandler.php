<?php
/**
 * Created by PhpStorm.
 * User: Justas
 * Date: 2019-01-04
 * Time: 18:03
 */
declare(strict_types=1);


namespace App\Http\Handlers\NotesHandlers;


use App\Http\Controllers\Controller;
use App\Http\Handlers\HandlerValidator;
use App\Http\Requests\NoteRequests\EditNoteRequest;
use App\Note;
use App\NotePermission;
use Illuminate\Http\JsonResponse;


class UpdateNoteHandler extends Controller
{
    /**
     * @var EditNoteRequest
     */
    protected $request;

    /**
     * @var Note
     */
    protected $note;

    public function __invoke($id, EditNoteRequest $editNoteRequest)
    {
        $this->request = $editNoteRequest;
        $this->note = Note::find($id);

        (new HandlerValidator($this->note, $this->request))->validateData();

        $this->note->setPropertiesFromRequest($editNoteRequest);
        $this->note->save();

        NotePermission::updatePermissions($this->note, $editNoteRequest);

        return response()->json(['note' => $this->note], JsonResponse::HTTP_OK);
    }


}
