<?php
/**
 * Created by PhpStorm.
 * User: Justas
 * Date: 2019-01-04
 * Time: 14:37
 */
declare(strict_types=1);


namespace App\Http\Handlers\NotesHandlers;


use App\Http\Controllers\Controller;
use App\Http\Handlers\HandlerValidator;
use App\Http\Requests\NoteRequests\DeleteNoteRequest;
use App\Note;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;

class DeleteNoteHandler extends Controller
{
    /**
     * @var Note
     */
    protected $note;

    /**
     * @var Request
     */
    protected $request;
    public function __invoke($id, DeleteNoteRequest $request) : JsonResponse
    {
        $this->request = $request;
        $this->note = Note::find($id);

        $this->checkIfExists();

        (new HandlerValidator($this->note, $this->request))->validateData();

        $this->note->delete();

        return response()->json([
            'note' => $this->note,
            'actions' => [
                'create' => Route('create-note'),
            ],
        ], JsonResponse::HTTP_OK);
    }

    private function checkIfExists() {
        if ($this->note == null) {
            throw new HttpResponseException(response()->json([
                'errors' => ["Note not found"]
            ], JsonResponse::HTTP_NOT_FOUND));
        }
    }

}
