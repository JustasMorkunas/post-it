<?php
/**
 * Created by PhpStorm.
 * User: Justas
 * Date: 2019-10-29
 * Time: 19:24
 */
declare(strict_types=1);


namespace App\Http\Requests\NoteRequests;


use App\Http\Requests\Contracts\APIRequest;

class DeleteNoteRequest extends APIRequest
{
    /**
     * @return String[]
     */
    public function rules()
    {
        return [];
    }
}
