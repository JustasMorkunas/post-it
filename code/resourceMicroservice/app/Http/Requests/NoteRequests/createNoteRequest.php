<?php
/**
 * Created by PhpStorm.
 * User: Justas
 * Date: 2019-01-02
 * Time: 15:23
 */
declare(strict_types=1);


namespace App\Http\Requests\NoteRequests;


use App\Http\Requests\Contracts\APIRequest;

class createNoteRequest extends APIRequest
{
    /**
     * @return String[]
     */
    public function rules()
    {
        return [
            'project_id' => 'exists:projects,id',
            'status' => 'required',
            'title' => 'required|min:3',
            'content' => 'required|min:3',
            'access_permissions' => 'integer|between:0,7',
            'shared_with' => 'array',
            'shared_with.*' => 'exists:users,email',
            'deadline' => '',
            'last_modified_by' => 'numeric',
        ];
    }

    public function getProjectId()
    {
        return $this->input('project_id', null);
    }

    public function getStatus(): string
    {
        return $this->input('status');
    }

    public function getTitle(): string
    {
        return $this->input('title');
    }

    public function getContentInput(): string
    {
        return $this->input('content');
    }

    public function getAccessPermissions()
    {
        return $this->input('access_permissions');
    }

    public function getSharedWith()
    {
        return $this->input('shared_with', []);
    }

    public function getDeadline()
    {
        if($this->input('deadline'))
            return new \DateTime($this->input('deadline'));
        return null;
    }

    public function getLastModifiedBy()
    {
        return $this->input('last_modified_by', null);
    }
}
