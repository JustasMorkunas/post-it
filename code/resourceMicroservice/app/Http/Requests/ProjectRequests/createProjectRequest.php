<?php
/**
 * Created by PhpStorm.
 * User: Justas
 * Date: 2018-12-28
 * Time: 17:19
 */
declare(strict_types=1);


namespace App\Http\Requests\ProjectRequests;


use App\Http\Requests\Contracts\APIRequest;

class createProjectRequest extends APIRequest
{
    /**
     * @return string[]
     */
    public function rules()
    {
        return [
            'title' => 'required|min:3',
            'about' => '',
            'access_permissions' => '',
            'parent_id' => '',
        ];
    }

    public function getTitle() : string
    {
        return $this->input('title');
    }

    public function getAbout() : string
    {
        return $this->input('about', '');
    }

    public function getAccessPermissions() : string
    {
        return $this->input('access_permissions', '7');
    }

    public function getParentId()
    {
        if($this->input('parent_id') == 0)
            return null;
        return $this->input('parent_id', null);
    }
}
