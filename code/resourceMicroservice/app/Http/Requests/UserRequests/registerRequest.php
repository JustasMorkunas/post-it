<?php
/**
 * Created by PhpStorm.
 * User: Justas
 * Date: 2018-12-06
 * Time: 13:48
 */
declare(strict_types=1);


namespace App\Http\Requests\UserRequests;


use App\DTO\UserDTO;
use App\Http\Requests\Contracts\APIRequest;
use Illuminate\Support\Facades\Hash;

class registerRequest extends APIRequest
{
    public function rules()
    {
        return [
            'name' => 'required',
            'lastname' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'password_confirm' => 'required_with:password|same:password',
        ];
    }

    public function getName() : string
    {
        return ucfirst(strtolower($this->input('name')));
    }

    public function getLastname() : string
    {
        return ucfirst(strtolower($this->input('lastname')));
    }

    public function getEmail() : string
    {
        return $this->input('email');
    }

    public function getUserPassword() : string
    {
        return Hash::make($this->input('password', null));
    }

    public function getUserDTO() : UserDTO
    {
        return (new UserDTO())->setFirstname($this->getName())
            ->setLastname($this->getLastname())
            ->setEmail($this->getEmail())
            ->setPassword($this->getUserPassword());
    }
}
