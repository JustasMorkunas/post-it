<?php
/**
 * Created by PhpStorm.
 * User: Justas
 * Date: 2019-05-30
 * Time: 18:31
 */
declare(strict_types=1);


namespace App\Http\Requests\UserRequests;


class editRequest extends registerRequest
{

    public function rules()
    {
        return [
            'name' => 'required',
            'lastname' => 'required',
            'email' => 'required|email',
            'password' => '',
            'password_confirm' => 'same:password',
        ];
    }
}
