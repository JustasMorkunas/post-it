<?php
/**
 * Created by PhpStorm.
 * User: Justas
 * Date: 2018-12-27
 * Time: 19:12
 */
declare(strict_types=1);


namespace App\Http\Requests\UserRequests;


use App\Http\Requests\Contracts\APIRequest;

class loginRequest extends APIRequest
{

    /**
     * @return string[]
     */
   public function rules()
   {
       return [
           'email' => 'required|email|exists:users',
           'password' => 'required',
       ];
   }

   public function getEmail() : string
   {
       return $this->input('email');
   }

   public function getUserPassword()
   {
       return $this->input('password');
   }

}
