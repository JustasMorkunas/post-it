<?php
/**
 * Created by PhpStorm.
 * User: Justas
 * Date: 2018-12-28
 * Time: 16:47
 */
declare(strict_types=1);


namespace App\Http\Requests\Contracts;


use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;

class APIRequest extends FormRequest
{

    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();

        throw new HttpResponseException(response()->json($errors, JsonResponse::HTTP_BAD_REQUEST));
    }

}
