<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\TokenBlacklistedException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;

class JwtMiddleware extends BaseMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try{
            $token = JWTAuth::getToken();
            $data = JWTAuth::getPayload($token)->toArray();
            $request->attributes->set('user_id', $data['user_id']);
        } catch (\Exception $exception) {
            $this->errorHandler($exception);
        }
        return $next($request);
    }

    protected function errorHandler(\Exception $exception)
    {
        $message = 'Token doesn\'t exist.';
        if($exception instanceof TokenInvalidException)
            $message = 'Token is invalid.';
        elseif($exception instanceof TokenExpiredException)
            $message = 'Token is expired.';
        elseif ($exception instanceof TokenBlacklistedException)
            $message = 'Token is blacklisted.';

        throw new HttpResponseException(response()->json([
            'errors' => ['token' => [$message]],
        ], JsonResponse::HTTP_UNAUTHORIZED));

    }
}
