<?php

namespace App;


use App\Http\Requests\ProjectRequests\createProjectRequest;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

/**
 * @method static find($id)
 * @method static where(string $string, $get)
 */
class Project extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'title', 'about', 'access_permissions', 'parent_id'
    ];

    public function setPropertiesFromRequest(createProjectRequest $createProjectRequest) : void
    {
        $this->title = $createProjectRequest->getTitle();
        $this->about = $createProjectRequest->getAbout();
        $this->owner_id = $createProjectRequest->attributes->get('user_id');
        $this->access_permissions = $createProjectRequest->getAccessPermissions();
        $this->parent_id = $createProjectRequest->getParentId();
    }

    public function setDefaults(Request $request) : void
    {
        $this->title = 'Home';
        $this->about = '';
        $this->owner_id = $request->attributes->get('user_id');
        $this->access_permissions = '777';
        $this->parent_id = null;
    }
}
