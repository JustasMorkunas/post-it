<?php
/**
 * Created by PhpStorm.
 * User: Justas
 * Date: 2019-10-29
 * Time: 19:58
 */
declare(strict_types=1);


namespace App\Exceptions;


use Throwable;

class ProcessingException extends \Exception
{

    function __construct(string $message = "", int $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

    /*
     * Method rewritten for testing purposes.
     */
    public function getMsg() {
        return parent::getMessage();
    }

}
