<?php
/**
 * Created by PhpStorm.
 * User: Justas
 * Date: 2019-10-29
 * Time: 20:21
 */
declare(strict_types=1);


namespace App\Exceptions;


use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CustomExceptionHandler
{

    public function handle($exception) {
        if ($exception instanceof NotFoundHttpException) {
            return response()->json([
                'errors' => ['Route not found']
            ], JsonResponse::HTTP_BAD_REQUEST);
        }

        if ($exception instanceof ProcessingException) {
            return response()->json([
                'errors' => [$exception->getMsg()]
            ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
        }
        return null;
    }

}
