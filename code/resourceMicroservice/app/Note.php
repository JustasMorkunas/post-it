<?php

namespace App;

use App\Http\Requests\Contracts\APIRequest;
use App\Http\Requests\NoteRequests\createNoteRequest;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * @method static find($id)
 * @method static where(string $string, $id)
 */
class Note extends Model
{
    protected $fillable = [
        'project_id', 'status', 'title', 'content', 'access_permissions', 'deadline', 'last_modified_by'
    ];

    public function setPropertiesFromRequest(APIRequest $request)
    {

        if($request instanceof createNoteRequest) {
            $this->setPropertiesFromCreateRequest($request);
        } else {
            $this->project_id = $request->getProjectId();
            $this->status = $request->getStatus();
            $this->title = $request->getTitle();
            $this->content = $request->getContentInput();
            $this->deadline = $request->getDeadline();
            return $this;
        }

    }

    protected function setPropertiesFromCreateRequest(createNoteRequest $request) : Note
    {
        $this->project_id = $request->getProjectId();
        $this->status = $request->getStatus();
        $this->title = $request->getTitle();
        $this->content = $request->getContentInput();
        $this->deadline = $request->getDeadline();
        $this->last_modified_by = $request->attributes->get('user_id');


        $this->owner_id = $request->attributes->get('user_id');

        return $this;
    }

    /**
     * @param $permissions Collection
     * @return Collection
     */
    public static function getSharedNotes($permissions) {

        if ($permissions == null)
            return [];

        $noteIds = [];
        foreach ($permissions as $permission) {
            $noteIds[] = $permission->note_id;
        }

        return  Note::whereIn('id', $noteIds)->get();
    }
}
