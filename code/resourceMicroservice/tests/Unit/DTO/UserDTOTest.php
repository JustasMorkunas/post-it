<?php
/**
 * Created by PhpStorm.
 * User: Justas
 * Date: 2019-11-04
 * Time: 18:40
 */
declare(strict_types=1);


namespace resourceMicroservice\tests\Unit\DTO;


use App\DTO\UserDTO;
use App\User;
use Tests\TestCase;

class UserDTOTest extends TestCase
{

    /**
     * @var UserDTO
     */
    private $userDTO;

    /**
     * @var User
     */
    private $user;

    public function setUp()
    {
        parent::setUp();
        $this->userDTO = new UserDTO();
        $this->user = factory(User::class)->make();
    }


    public function testSetFirstName()
    {
        $this->userDTO->setFirstname($this->user->name);

        $this->assertEquals($this->user->name, $this->userDTO->getFirstname());
    }

    public function testSetLastName()
    {
        $this->userDTO->setLastname($this->user->lastname);

        $this->assertEquals($this->user->lastname, $this->userDTO->getLastname());
    }

    public function testSetEmail()
    {
        $this->userDTO->setEmail($this->user->email);

        $this->assertEquals($this->user->email, $this->userDTO->getEmail());
    }


    public function testSetPassword()
    {
        $passwordString = str_random(6);
        $this->userDTO->setPassword($passwordString);

        $this->assertEquals($passwordString, $this->userDTO->getPassword());
    }

}
