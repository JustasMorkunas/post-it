<?php

namespace Tests\Unit;

use App\Note;
use App\Project;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;
use Tymon\JWTAuth\Facades\JWTAuth;


class EndpointTests extends TestCase
{

    const VALID_PASSWORD = 'testpass';

    private $user;
    private $note;
    private $project;
    private $token;


    /**
     * SetUp
     */
    public function setUp()
    {
        parent::setUp();
        $this->user = factory(User::class)->make([
            'password' => Hash::make(self::VALID_PASSWORD)
        ]);
        $this->user->save();

        $this->project = factory(Project::class)->make([
            'owner_id' => $this->user->id,
            'parent_id' => null,
        ]);
        $this->project->save();

        $this->note = factory(Note::class)->make([
            'owner_id' => $this->user->id,
            'project_id' => $this->project->id,
        ]);
        $this->note->save();

        $this->token = $this->getAuthHeader($this->user);
    }


    /**
     * TearDown
     */
    public function tearDown()
    {
        parent::tearDown();
        $this->note->delete();
        $this->project->delete();
        $this->user->delete();
    }



    /**
     * Pp\Http\Handlers\ProjectsHandlers\GetProjectHandler
     * @group getProjectUnauthorized
     * @return void
     */
    public function testGetProjectHandlerWithError()
    {
        $testUser = factory(User::class)->make([]);
        $testUser->save();

        $this->project->owner_id = $testUser->id;
        $this->project->save();

        $response = $this->json('GET', '/projects/'.$this->project->id, [], $this->token);

        $response->assertStatus(JsonResponse::HTTP_FORBIDDEN);
        $response->assertJson([
            'errors' => ['You don\'t have permission to access this project'],
        ]);

        $testUser->delete();

    }



    /**
     * Pp\Http\Handlers\ProjectsHandlers\GetProjectHandler
     * @group getProject
     * @return void
     */
    public function testGetProjectHandler()
    {
        $response = $this->json('GET', '/projects/'.$this->project->id, [], $this->token);

        $response->assertOk();
        $this->assertObjectHasAttribute('project', json_decode($response->getContent()));

        $responseProject = json_decode($response->getContent())->project;
        $this->assertEquals($this->project->owner_id, $responseProject->owner_id);
        $this->assertEquals($this->project->parent_id, $responseProject->parent_id);
        $this->assertEquals($this->project->title, $responseProject->title);
        $this->assertEquals($this->project->about, $responseProject->about);

    }



    /**
     * Pp\Http\Handlers\ProjectsHandlers\CreateProjectHandler
     * @group createProject
     * @return void
     */
    public function testCreateProjectHandler()
    {
        $testProject = factory(Project::class)->make([
            'owner_id' => $this->user->id,
            'parent_id' => null,
        ]);

        $this->assertDatabaseMissing('projects', $testProject->toArray());

        $response = $this->json('POST', '/projects', $testProject->toArray(), $this->token);

        $response->assertOk();
        $this->assertObjectHasAttribute('project', json_decode($response->getContent()));
        $this->assertDatabaseHas('projects', $testProject->toArray());
    }



    /**
     * Pp\Http\Handlers\ProjectsHandlers\DeleteProjectHandler
     * @group deleteProjectUnauthorized
     * @return void
     */
    public function testUnauthorizedDeleteProjectHandlerr()
    {
        $testUser = factory(User::class)->make([]);
        $testUser->save();

        $this->project->owner_id = $testUser->id;
        $this->project->save();

        $this->assertDatabaseHas('projects', $this->project->toArray());

        $response = $this->json('DELETE', '/projects/'.$this->project->id, [], $this->token);

        $response->assertStatus(JsonResponse::HTTP_FORBIDDEN);
        $response->assertJson([
            'errors' => ['You don\'t have permission to access this project'],
        ]);
        $this->assertDatabaseHas('projects', $this->project->toArray());

        $testUser->delete();
    }



    /**
     * Pp\Http\Handlers\ProjectsHandlers\DeleteProjectHandler
     * @group deleteProject
     * @return void
     */
    public function testDeleteProjectHandler()
    {

        $this->assertDatabaseHas('projects', $this->project->toArray());

        $response = $this->json('DELETE', '/projects/'.$this->project->id, [], $this->token);

        $response->assertOk();
        $this->assertDatabaseMissing('projects', $this->project->toArray());

    }



    /**
     * Pp\Http\Handlers\ProjectsHandlers\UpdateProjectHandler
     * @group updateProjectUnauthorized
     * @return void
     */
    public function testUnauthorizedUpdateProjectHandler()
    {
        $testUser = factory(User::class)->make([]);
        $testUser->save();

        $this->project->owner_id = $testUser->id;
        $this->project->save();

        $expected = [
            "title" => str_random(5),
            "about" => str_random(15),
            "access_permissions" => "7",
            "parent_id" => null
        ];

        $this->assertDatabaseMissing('projects', $expected);

        $response = $this->json('PUT', '/projects/'.$this->project->id, $expected, $this->token);

        $response->assertStatus(JsonResponse::HTTP_FORBIDDEN);
        $response->assertJson([
            'errors' => ['You don\'t have permission to access this project'],
        ]);
        $this->assertDatabaseMissing('projects', $expected);
        $testUser->delete();
    }



    /**
     * Pp\Http\Handlers\ProjectsHandlers\UpdateProjectHandler
     * @group updateProject
     * @return void
     */
    public function testUpdateProjectHandler()
    {
        $expected = [
            "title" => str_random(5),
            "about" => str_random(15),
            "access_permissions" => "7",
            "parent_id" => null
        ];

        $this->assertDatabaseMissing('projects', $expected);

        $response = $this->json('PUT', '/projects/'.$this->project->id, $expected, $this->token);

        $response->assertOk();
        $this->assertObjectHasAttribute('project', json_decode($response->getContent()));
        $this->assertDatabaseHas('projects', $expected);
    }



    /**
     * Pp\Http\Handlers\NotesHandlers\CreateNoteHandler
     * @group createNote
     * @return void
     */
    public function testCreateNoteHandler()
    {
        $testNote = factory(Note::class)->make([
            'owner_id' => $this->user->id,
            'project_id' => $this->project->id,
            'deadline' => null,
            'last_modified_by' => $this->user->id
        ]);

        $this->assertDatabaseMissing('notes',  $testNote->toArray());

        $response = $this->json('POST', '/notes', [
            "project_id" => $testNote->project_id,
            "status" => $testNote->status,
            "title" => $testNote->title,
            "content" => $testNote->content,
            "shared_with" => []
        ], $this->token);


        $response->assertOk();
        $this->assertDatabaseHas('notes', $testNote->toArray());

    }



    /**
     * Pp\Http\Handlers\NotesHandlers\GetNoteHandler
     * @group getNoteUnauthorized
     * @return void
     */
    public function testUnauthorizedGetNote()
    {
        $testUser = factory(User::class)->make([]);
        $testUser->save();

        $this->note->owner_id = $testUser->id;
        $this->note->save();

        $response = $this->json('GET', '/notes/'.$this->note->id, [], $this->token);

        $response->assertStatus(JsonResponse::HTTP_FORBIDDEN);
        $response->assertJson([
            'errors' => ['You don\'t have permission to access this note'],
        ]);

    }



    /**
     * Pp\Http\Handlers\NotesHandlers\GetNoteHandler
     * @group getNote
     * @return void
     */
    public function testGetNoteHandler()
    {
        $response = $this->json('GET', '/notes/'.$this->note->id, [], $this->token);

        $response->assertOk();
        $this->assertObjectHasAttribute('note', json_decode($response->getContent()));
        $noteResponseData = json_decode($response->getContent())->note;

        $this->assertEquals($this->note->id, $noteResponseData->id);
        $this->assertEquals($this->note->project_id, $noteResponseData->project_id);
        $this->assertEquals($this->note->owner_id, $noteResponseData->owner_id);
        $this->assertEquals($this->note->status, $noteResponseData->status);
        $this->assertEquals($this->note->title, $noteResponseData->title);
        $this->assertEquals($this->note->content, $noteResponseData->content);
    }



    /**
     * Pp\Http\Handlers\NotesHandlers\DeleteNoteHandler
     * @group deleteNoteUnauthorized
     * @return void
     */
    public function testUnauthorizedDeleteNoteHandler()
    {
        $testUser = factory(User::class)->make([]);
        $testUser->save();

        $this->note->owner_id = $testUser->id;
        $this->note->save();

        $this->assertDatabaseHas('notes', $this->note->toArray());

        $response = $this->json('DELETE', '/notes/'.$this->note->id, [], $this->token);
        $response->assertStatus(JsonResponse::HTTP_FORBIDDEN);
        $response->assertJson([
            'errors' => ['You don\'t have permission to access this note'],
        ]);
        $this->assertDatabaseHas('notes', $this->note->toArray());

        $testUser->delete();

    }



    /**
     * Pp\Http\Handlers\NotesHandlers\DeleteNoteHandler
     * @group deleteNote
     * @return void
     */
    public function testDeleteNoteHandler()
    {
        $this->assertDatabaseHas('notes', $this->note->toArray());

        $response = $this->json('DELETE', '/notes/'.$this->note->id, [], $this->token);

        $response->assertOk();
        $this->assertObjectHasAttribute('note', json_decode($response->getContent()));
        $this->assertDatabaseMissing('notes', $this->note->toArray());

    }



    /**
     * Pp\Http\Handlers\NotesHandlers\UpdateNoteHandler
     * @group updateNoteUnauthorized
     * @return void
     */
    public function testUnauthorizedUpdateNoteHandler()
    {
        $testUser = factory(User::class)->make([]);
        $testUser->save();

        $this->note->owner_id = $testUser->id;
        $this->note->save();

        $initial = $this->note->toArray();

        $this->assertDatabaseHas('notes', $initial);

        $updated = [
            "project_id" => $this->note->project_id,
            "status" => $this->note->status,
            "title" => str_random(8),
            "content" => str_random(15),
            "deadline" => '2020-08-16',
            "last_modified_by" => $this->user->id
        ];

        $response = $this->json('PUT', '/notes/'.$this->note->id, $updated, $this->token);

        $response->assertStatus(JsonResponse::HTTP_FORBIDDEN);
        $this->assertDatabaseHas('notes', $initial);
        $this->assertDatabaseMissing('notes', $updated);
        $testUser->delete();
    }



    /**
     * Pp\Http\Handlers\NotesHandlers\UpdateNoteHandler
     * @group updateNote
     * @return void
     */
    public function testUpdateNoteHandler()
    {
        $initial = $this->note->toArray();

        $this->assertDatabaseHas('notes', $initial);

        $updated = [
            "project_id" => $this->note->project_id,
            "status" => $this->note->status,
            "title" => str_random(8),
            "content" => str_random(15),
            "deadline" => '2020-08-16',
            "last_modified_by" => $this->user->id
        ];

        $response = $this->json('PUT', '/notes/'.$this->note->id, $updated, $this->token);

        $response->assertOk();
        $this->assertDatabaseHas('notes', $updated);
        $this->assertDatabaseMissing('notes', $initial);
    }



    /**
     * Pp\Http\Handlers\UserHandlers\getRequestHandler
     * @group getProfileUnauthorized
     * @return void
     */
    public function testUnauthorizedGetRequestHandler()
    {
        $response = $this->json('GET', '/user', []);

        $response->assertStatus(JsonResponse::HTTP_UNAUTHORIZED);
    }



    /**
     * Pp\Http\Handlers\UserHandlers\getRequestHandler
     * @group getProfile
     * @return void
     */
    public function testGetProfileRequestHandler()
    {
        $response = $this->json('GET', '/user', [], $this->token);

        $response->assertOk();

        $responseJson = (array) json_decode($response->getContent());
        $this->assertArrayHasKey('user', $responseJson);

        $userArray = (array) $responseJson['user'];
        $this->assertArraySubset($this->user->toArray(), $userArray);
    }



    /**
     * Pp\Http\Handlers\UserHandlers\editRequestHandler
     * @group editUserUnauthorized
     * @return void
     */
    public function testUnauthorizedEditProfileRequestHandler()
    {
        $unauthorizedUser = factory(User::class)->make([]);
        $unauthorizedUser->save();

        $initial = [
            'email' => $unauthorizedUser->email,
            'name' => $unauthorizedUser->name,
            'lastname' => $unauthorizedUser->lastname
        ];

        $this->assertDatabaseHas('users', $initial);

        $expected = [
            "name" => "Random",
            "lastname" => "Random",
            "email" => "geoffrey14@beatty.com",
        ];

        $response = $this->json('POST', '/user/'.$unauthorizedUser->id, $expected, $this->token);

        $response->assertStatus(JsonResponse::HTTP_FORBIDDEN);
        $response->assertJson([
            'errors' => ['You don\'t have permission to access this user data'],
        ]);
        $this->assertDatabaseMissing('users', $expected);
        $this->assertDatabaseHas('users', $initial);
        $unauthorizedUser->delete();
    }



    /**
     * Pp\Http\Handlers\UserHandlers\editRequestHandler
     * @group editUser
     * @return void
     */
    public function testEditProfileRequestHandler()
    {

        $initial = [
            'email' => $this->user->email,
            'name' => $this->user->name,
            'lastname' => $this->user->lastname
        ];

        $this->assertDatabaseHas('users', $initial);

        $expected = [
            "name" => "Random",
            "lastname" => "Random",
            "email" => "geoffrey14@beatty.com",
        ];

        $response = $this->json('POST', '/user/'.$this->user->id, $expected, $this->token);

        $response->assertStatus(200);
        $this->assertObjectHasAttribute('user', json_decode($response->getContent()));
        $this->assertDatabaseHas('users', $expected);
        $this->assertDatabaseMissing('users', $initial);
    }



    /**
     * Pp\Http\Handlers\UserHandlers\registerRequestHandler
     * @group registerTest
     * @return void
     */
    public function testRegisterRequestHandler()
    {
        $user = factory(User::class)->make(['password' => EndpointTests::VALID_PASSWORD]);
        $response = $this->json('POST', '/register', [
            "name" => $user->name,
            "lastname" => $user->lastname,
            "email" => $user->email,
            "password" => EndpointTests::VALID_PASSWORD,
            "password_confirm" => EndpointTests::VALID_PASSWORD
        ]);

        $response->assertStatus(200);
        $response->assertJson([
            'message' => 'User successfully created'
        ]);
        $this->assertObjectHasAttribute('token', json_decode($response->getContent()));
        $this->assertDatabaseHas('users', [
            'name' => $user->name,
            'lastname' => $user->lastname,
            'email' => $user->email,
        ]);
    }



    /**
     * Pp\Http\Handlers\UserHandlers\loginRequestHandler
     * @group loginTestFailed
     * @return void
     */
    public function testLoginRequestHandlerWithError()
    {
        $response = $this->json('POST', '/login', [
            "email" => $this->user->email,
            "password" => str_random(6),
        ]);

        $response->assertStatus(JsonResponse::HTTP_UNAUTHORIZED);
        $response->assertJson([
            'error'=>['password' => ['Incorrect password.']],
        ]);
    }



    /**
     * Pp\Http\Handlers\UserHandlers\loginRequestHandler
     * @group loginTest
     * @return void
     */
    public function testLoginRequestHandler()
    {

        $response = $this->json('POST', '/login', [
            "email" => $this->user->email,
            "password" => self::VALID_PASSWORD
        ]);

        $response->assertOk();
        $this->assertObjectHasAttribute('token', json_decode($response->getContent()));

    }



    private function getAuthHeader(User $user)
    {
        $token = 'Bearer ' . JWTAuth::customClaims(['user_id' => $user->id])
                ->attempt(['email' => $user->email, 'password' => EndpointTests::VALID_PASSWORD]);

        return ['Authorization' => $token];
    }

}
