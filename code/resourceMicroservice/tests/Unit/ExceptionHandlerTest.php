<?php
/**
 * Created by PhpStorm.
 * User: Justas
 * Date: 2019-10-29
 * Time: 20:02
 */
declare(strict_types=1);


namespace resourceMicroservice\tests\Unit;


use App\Exceptions\ProcessingException;
use App\Exceptions\CustomExceptionHandler;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Tests\TestCase;

class ExceptionHandlerTest extends TestCase
{
    private $handler;

    protected function setUp()
    {
        parent::setUp();
        $this->handler = new CustomExceptionHandler();
    }

    public function testProcessingExceptionHandling()
    {

        $exception = $this->createMock(ProcessingException::class);
        $exception->method("getMsg")
            ->will($this->returnValue("Processing error"));

        $response = $this->handler->handle($exception);

        $this->assertEquals(JsonResponse::HTTP_UNPROCESSABLE_ENTITY, $response->getStatusCode());
        $this->assertEquals(json_encode(['errors' => ['Processing error']]), $response->getContent());
    }

    public function testNotFoundHttpExceptionHandling()
    {
        $exception = $this->createMock(NotFoundHttpException::class);

        $response = $this->handler->handle($exception);

        $this->assertEquals(JsonResponse::HTTP_BAD_REQUEST, $response->getStatusCode());
        $this->assertJson(json_encode(['errors' => ['Route not found']]), $response->getContent());
    }

}
