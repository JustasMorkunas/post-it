<?php
/**
 * Created by PhpStorm.
 * User: Justas
 * Date: 2019-11-05
 * Time: 20:23
 */
declare(strict_types=1);


namespace resourceMicroservice\tests\Unit\Handlers\ModelValidators;


use App\Exceptions\ProcessingException;
use App\Http\Handlers\ModelValidators\ConcreteCreators\NoteValidatorCreator;
use App\Http\Handlers\ModelValidators\ConcreteCreators\ProjectValidatorCreator;
use App\Http\Handlers\ModelValidators\ConcreteCreators\UserValidatorCreator;
use App\Http\Handlers\ModelValidators\ValidatorFactoryMethod;
use App\Note;
use App\Project;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Tests\TestCase;

class ValidationFactoryTest extends TestCase
{
    public function testNoteValidatorCreation()
    {
        $factory = new ValidatorFactoryMethod(
            $this->createMock(Note::class),
            $this->createMock(Request::class)
        );

        $validator = $factory->getValidator();

        $this->assertTrue($validator instanceof NoteValidatorCreator);
    }

    public function testProjectValidatorCreation()
    {
        $factory = new ValidatorFactoryMethod(
            $this->createMock(Project::class),
            $this->createMock(Request::class)
        );

        $validator = $factory->getValidator();

        $this->assertTrue($validator instanceof ProjectValidatorCreator);
    }

    public function testUserValidatorCreation()
    {
        $factory = new ValidatorFactoryMethod(
            $this->createMock(User::class),
            $this->createMock(Request::class)
        );

        $validator = $factory->getValidator();

        $this->assertTrue($validator instanceof UserValidatorCreator);
    }

    public function testUnknownModelValidatorCreation()
    {
        $factory = new ValidatorFactoryMethod(
            $this->createMock(Model::class),
            $this->createMock(Request::class)
        );

        try {
            $factory->getValidator();
            $this->fail("ProcessingException expected, but no exception were thrown");
        } catch (\Exception $e) {
            $this->assertTrue($e instanceof ProcessingException);
        }

    }
}
