<?php
/**
 * Created by PhpStorm.
 * User: Justas
 * Date: 2019-11-05
 * Time: 20:01
 */
declare(strict_types=1);


namespace resourceMicroservice\tests\Unit\Handlers\ModelValidators\ConcreteCreators;


use App\Http\Handlers\ModelValidators\ConcreteCreators\ProjectValidatorCreator;
use App\Project;
use App\User;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\ParameterBag;
use Tests\TestCase;

class ProjectValidatorTest extends TestCase
{

    private $user;
    private $project;

    public function setUp()
    {
        parent::setUp();
        $this->user = factory(User::class)->make(['id' => 1]);
        $this->project = factory(Project::class)->make(['owner_id' => $this->user->id]);
    }

    public function testUnauthorizedOwnerValidation() {
        $attributes = $this->createMock(ParameterBag::class);
        $attributes->expects($this->any())
            ->method("get")
            ->with($this->equalTo('user_id'))
            ->will($this->returnValue($this->user->id + 1));

        $request = $this->createMock(Request::class);
        $request->attributes = $attributes;

        $handler = new ProjectValidatorCreator($this->project, $request);

        try {
            $handler->validateOwner();
            $this->fail("Expected HttpResponseException, but no exception was thrown");
        } catch (\Exception $e) {
            if ($e instanceof HttpResponseException) {
                $response = $e->getResponse();
                $this->assertEquals(JsonResponse::HTTP_FORBIDDEN, $response->getStatusCode());
                $expectedResponse = json_encode([
                    'errors' => ['You don\'t have permission to access this project'],
                ]);
                $this->assertEquals($expectedResponse, $response->getContent());
            } else {
                $this->fail("Expected HttpResponseException, but different exception was thrown");
            }
        }
    }

    public function testAuthorizedOwnerValidation() {
        $attributes = $this->createMock(ParameterBag::class);
        $attributes->expects($this->any())
            ->method("get")
            ->with($this->equalTo('user_id'))
            ->will($this->returnValue($this->user->id));

        $request = $this->createMock(Request::class);
        $request->attributes = $attributes;

        $handler = new ProjectValidatorCreator($this->project, $request);

        try {
            $handler->validateOwner();
            $this->assertTrue(true);
        } catch (\Exception $e) {
            $this->fail("Nothing was expected, but exception was thrown");
        }
    }
}
