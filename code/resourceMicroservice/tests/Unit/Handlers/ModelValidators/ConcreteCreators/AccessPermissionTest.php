<?php
/**
 * Created by PhpStorm.
 * User: Justas
 * Date: 2019-11-04
 * Time: 18:55
 */
declare(strict_types=1);


namespace resourceMicroservice\tests\Unit\Handlers\ModelValidators\ConcreteCreators;


use App\Http\Handlers\ModelValidators\ConcreteCreators\AccessPermission;
use Tests\TestCase;
use UnexpectedValueException;

class AccessPermissionTest extends TestCase
{

    public function testUnsupportedPermissionCodeHandling() {
        try {
            AccessPermission::getPermissions(-1);
            $this->fail("Exception expected, but no exception was thrown");
        } catch (\Exception $exception) {
            $this->assertTrue($exception instanceof UnexpectedValueException);
        }
    }

    public function testGetPermissionsForCode7()
    {
        $expectedResult = [
            AccessPermission::VIEW,
            AccessPermission::EDIT,
            AccessPermission::DELETE
        ];

        $result = AccessPermission::getPermissions(7);

        $this->compareArrays($expectedResult, $result);
    }

    public function testGetPermissionForCode6()
    {
        $expectedResult = [
            AccessPermission::VIEW,
            AccessPermission::EDIT
        ];

        $result = AccessPermission::getPermissions(6);

        $this->compareArrays($expectedResult, $result);
    }


    public function testGetPermissionForCode5()
    {
        $expectedResult = [
            AccessPermission::VIEW,
            AccessPermission::DELETE
        ];

        $result = AccessPermission::getPermissions(5);

        $this->compareArrays($expectedResult, $result);
    }

    public function testGetPermissionForCode4()
    {
        $expectedResult = [
            AccessPermission::VIEW
        ];

        $result = AccessPermission::getPermissions(4);

        $this->compareArrays($expectedResult, $result);
    }

    private function compareArrays($expected, $actual)
    {
        foreach ($expected as $expectedItem) {
            $this->assertTrue(in_array($expectedItem, $actual));
        }

        foreach ($actual as $actualItem) {
            $this->assertTrue(in_array($actualItem, $expected));
        }
    }

}
