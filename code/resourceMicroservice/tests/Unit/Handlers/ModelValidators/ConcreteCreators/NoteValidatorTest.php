<?php
/**
 * Created by PhpStorm.
 * User: Justas
 * Date: 2019-11-04
 * Time: 19:13
 */
declare(strict_types=1);


namespace resourceMicroservice\tests\Unit\Handlers\ModelValidators\ConcreteCreators;


use App\Exceptions\ProcessingException;
use App\Http\Handlers\ModelValidators\ConcreteCreators\NoteValidatorCreator;
use App\Http\Requests\NoteRequests\EditNoteRequest;
use App\Note;
use App\NotePermission;
use App\User;
use App\Http\Requests\NoteRequests\DeleteNoteRequest;
use App\Http\Requests\NoteRequests\GetNoteRequest;
use Symfony\Component\HttpFoundation\ParameterBag;
use Tests\TestCase;

class NoteValidatorTest extends TestCase
{

    private $user;
    private $note;

    public function setUp()
    {
        parent::setUp();
        $this->user = factory(User::class)->make([
            'id' => 1
        ]);

        $this->note = factory(Note::class)->make([
            'id' => 1,
            'owner_id' => $this->user->id
        ]);
    }

    public function testIsOwnerMethodForOwnerUser()
    {
        $attributes = $this->createMock(ParameterBag::class);
        $attributes->expects($this->any())
            ->method("get")
            ->with($this->equalTo('user_id'))
            ->will($this->returnValue($this->user->id));

        $request = $this->createMock(\Illuminate\Http\Request::class);
        $request->attributes = $attributes;

        $validator = new NoteValidatorCreator($this->note, $request);
        $this->assertTrue($validator->isOwner());
    }

    public function testIsOwnerForNonOwnerUser()
    {
        $attributes = $this->createMock(ParameterBag::class);
        $attributes->expects($this->any())
            ->method("get")
            ->with($this->equalTo('user_id'))
            ->will($this->returnValue(2));

        $request = $this->createMock(\Illuminate\Http\Request::class);
        $request->attributes = $attributes;

        $validator = new NoteValidatorCreator($this->note, $request);
        $this->assertFalse($validator->isOwner());
    }


    public function testIsActionAllowedWithNoPermissions()
    {
        $attributes = $this->createMock(ParameterBag::class);
        $attributes->expects($this->any())
            ->method("get")
            ->with($this->equalTo('user_id'))
            ->will($this->returnValue($this->user->id));

        $request = $this->createMock(\Illuminate\Http\Request::class);
        $request->attributes = $attributes;

        $validator = new NoteValidatorCreator($this->note, $request);

        $this->assertFalse($validator->isSharedWith(null));
    }

    public function testIsSharedWithForUnknownRequestType()
    {

        $attributes = $this->createMock(ParameterBag::class);
        $attributes->expects($this->any())
            ->method("get")
            ->with($this->equalTo('user_id'))
            ->will($this->returnValue($this->user->id));

        $request = $this->createMock(\Illuminate\Http\Request::class);
        $request->attributes = $attributes;

        $notePermission = $this->createNotePermission(7);

        $validator = new NoteValidatorCreator($this->note, $request);

        try {
            $validator->isSharedWith($notePermission);
            $this->fail("ProcessingException expected, but no exception was thrown");
        } catch (\Exception $e) {
            $this->assertTrue($e instanceof ProcessingException);
        }
    }

    public function testIsActionAllowedForEditRequestAndUserWith7Permission()
    {

        $attributes = $this->createMock(ParameterBag::class);
        $attributes->expects($this->any())
            ->method("get")
            ->with($this->equalTo('user_id'))
            ->will($this->returnValue($this->user->id));

        $request = $this->createMock(EditNoteRequest::class);
        $request->attributes = $attributes;

        $notePermission = $this->createNotePermission(7);
        $validator = new NoteValidatorCreator($this->note, $request);

        $this->assertTrue($validator->isSharedWith($notePermission));
    }

    public function testIsActionAllowedForEditRequestAndUserWith6Permission()
    {
        $attributes = $this->createMock(ParameterBag::class);
        $attributes->expects($this->any())
            ->method("get")
            ->with($this->equalTo('user_id'))
            ->will($this->returnValue($this->user->id));

        $request = $this->createMock(EditNoteRequest::class);
        $request->attributes = $attributes;

        $notePermission = $this->createNotePermission(6);
        $validator = new NoteValidatorCreator($this->note, $request);

        $this->assertTrue($validator->isSharedWith($notePermission));
    }

    public function testIsActionAllowedForEditRequestAndUserWith5Permission()
    {
        $attributes = $this->createMock(ParameterBag::class);
        $attributes->expects($this->any())
            ->method("get")
            ->with($this->equalTo('user_id'))
            ->will($this->returnValue($this->user->id));

        $request = $this->createMock(EditNoteRequest::class);
        $request->attributes = $attributes;

        $notePermission = $this->createNotePermission(5);
        $validator = new NoteValidatorCreator($this->note, $request);

        $this->assertFalse($validator->isSharedWith($notePermission));
    }

    public function testIsActionAllowedForEditRequestAndUserWith4Permission()
    {
        $attributes = $this->createMock(ParameterBag::class);
        $attributes->expects($this->any())
            ->method("get")
            ->with($this->equalTo('user_id'))
            ->will($this->returnValue($this->user->id));

        $request = $this->createMock(EditNoteRequest::class);
        $request->attributes = $attributes;

        $notePermission = $this->createNotePermission(4);
        $validator = new NoteValidatorCreator($this->note, $request);

        $this->assertFalse($validator->isSharedWith($notePermission));
    }

    public function testIsActionAllowedForDeleteRequestAndUserWith7Permission()
    {
        $attributes = $this->createMock(ParameterBag::class);
        $attributes->expects($this->any())
            ->method("get")
            ->with($this->equalTo('user_id'))
            ->will($this->returnValue($this->user->id));

        $request = $this->createMock(DeleteNoteRequest::class);
        $request->attributes = $attributes;

        $notePermission = $this->createNotePermission(7);
        $validator = new NoteValidatorCreator($this->note, $request);

        $this->assertTrue($validator->isSharedWith($notePermission));
    }

    public function testIsActionAllowedForDeleteRequestAndUserWith6Permission()
    {
        $attributes = $this->createMock(ParameterBag::class);
        $attributes->expects($this->any())
            ->method("get")
            ->with($this->equalTo('user_id'))
            ->will($this->returnValue($this->user->id));

        $request = $this->createMock(DeleteNoteRequest::class);
        $request->attributes = $attributes;

        $notePermission = $this->createNotePermission(6);
        $validator = new NoteValidatorCreator($this->note, $request);

        $this->assertFalse($validator->isSharedWith($notePermission));
    }

    public function testIsActionAllowedForDeleteRequestAndUserWith5Permission()
    {
        $attributes = $this->createMock(ParameterBag::class);
        $attributes->expects($this->any())
            ->method("get")
            ->with($this->equalTo('user_id'))
            ->will($this->returnValue($this->user->id));

        $request = $this->createMock(DeleteNoteRequest::class);
        $request->attributes = $attributes;

        $notePermission = $this->createNotePermission(5);
        $validator = new NoteValidatorCreator($this->note, $request);

        $this->assertTrue($validator->isSharedWith($notePermission));
    }

    public function testIsActionAllowedForDeleteRequestAndUserWith4Permission()
    {
        $attributes = $this->createMock(ParameterBag::class);
        $attributes->expects($this->any())
            ->method("get")
            ->with($this->equalTo('user_id'))
            ->will($this->returnValue($this->user->id));

        $request = $this->createMock(DeleteNoteRequest::class);
        $request->attributes = $attributes;

        $notePermission = $this->createNotePermission(4);
        $validator = new NoteValidatorCreator($this->note, $request);

        $this->assertFalse($validator->isSharedWith($notePermission));
    }

    public function testIsActionAllowedForGetRequestAndUserWith7Permission()
    {
        $attributes = $this->createMock(ParameterBag::class);
        $attributes->expects($this->any())
            ->method("get")
            ->with($this->equalTo('user_id'))
            ->will($this->returnValue($this->user->id));

        $request = $this->createMock(GetNoteRequest::class);
        $request->attributes = $attributes;

        $notePermission = $this->createNotePermission(7);
        $validator = new NoteValidatorCreator($this->note, $request);

        $this->assertTrue($validator->isSharedWith($notePermission));
    }

    public function testIsActionAllowedForGetRequestAndUserWith6Permission()
    {
        $attributes = $this->createMock(ParameterBag::class);
        $attributes->expects($this->any())
            ->method("get")
            ->with($this->equalTo('user_id'))
            ->will($this->returnValue($this->user->id));

        $request = $this->createMock(GetNoteRequest::class);
        $request->attributes = $attributes;

        $notePermission = $this->createNotePermission(6);
        $validator = new NoteValidatorCreator($this->note, $request);

        $this->assertTrue($validator->isSharedWith($notePermission));
    }

    public function testIsActionAllowedForGetRequestAndUserWith5Permission()
    {
        $attributes = $this->createMock(ParameterBag::class);
        $attributes->expects($this->any())
            ->method("get")
            ->with($this->equalTo('user_id'))
            ->will($this->returnValue($this->user->id));

        $request = $this->createMock(GetNoteRequest::class);
        $request->attributes = $attributes;

        $notePermission = $this->createNotePermission(5);
        $validator = new NoteValidatorCreator($this->note, $request);

        $this->assertTrue($validator->isSharedWith($notePermission));
    }

    public function testIsActionAllowedForGetRequestAndUserWith4Permission()
    {
        $attributes = $this->createMock(ParameterBag::class);
        $attributes->expects($this->any())
            ->method("get")
            ->with($this->equalTo('user_id'))
            ->will($this->returnValue($this->user->id));

        $request = $this->createMock(GetNoteRequest::class);
        $request->attributes = $attributes;

        $notePermission = $this->createNotePermission(4);
        $validator = new NoteValidatorCreator($this->note, $request);

        $this->assertTrue($validator->isSharedWith($notePermission));
    }

    private function createNotePermission($permissionCode): NotePermission
    {

        return factory(NotePermission::class)->make([
            'note_id' => $this->note->id,
            'user_id' => $this->user->id,
            'permission' => $permissionCode
        ]);

    }


}
