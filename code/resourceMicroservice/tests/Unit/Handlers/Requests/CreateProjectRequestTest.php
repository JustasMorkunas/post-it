<?php
/**
 * Created by PhpStorm.
 * User: Justas
 * Date: 2019-11-14
 * Time: 18:17
 */
declare(strict_types=1);


namespace resourceMicroservice\tests\Unit\Handlers\Requests;


use App\Http\Requests\ProjectRequests\createProjectRequest;
use App\Project;
use App\User;
use Tests\TestCase;
use Illuminate\Support\Facades\Validator;

class CreateProjectRequestTest extends TestCase
{

    public function testValidationSuccess()
    {
        $createProjectRequest = new createProjectRequest();

        $project = factory(Project::class)->make([
            'title' => str_random(6),
        ]);

        $validator = Validator::make($project->toArray(), $createProjectRequest->rules(), $createProjectRequest->messages());

        $this->assertFalse($validator->fails());

    }

    public function testTitleValidationFailure()
    {
        $createProjectRequest = new createProjectRequest();

        $project = factory(Project::class)->make([
            'title' => str_random(2)
        ]);

        $validator = Validator::make($project->toArray(), $createProjectRequest->rules(), $createProjectRequest->messages());

        $this->assertTrue($validator->fails());
    }

}
