<?php
/**
 * Created by PhpStorm.
 * User: Justas
 * Date: 2019-11-05
 * Time: 20:34
 */
declare(strict_types=1);


namespace resourceMicroservice\tests\Unit\Handlers\Requests\NoteRequests;


use App\Http\Requests\NoteRequests\createNoteRequest;

use App\Note;
use App\Project;
use App\User;
use Illuminate\Support\Facades\Validator;
use Tests\TestCase;

class CreateNoteRequestTest extends TestCase
{
    private $user;
    private $project;

    public function setUp()
    {
        parent::setUp();
        $this->user = factory(User::class)->make([]);
        $this->user->save();
        $this->project = factory(Project::class)->make(['owner_id' => $this->user->id, 'parent_id' => null]);
        $this->project->save();
    }

    public function tearDown()
    {
        parent::tearDown();
        $this->user->delete();
        $this->project->delete();
    }

    public function testProjectIdValidationSuccess()
    {
        $createNoteRequest = new createNoteRequest();


        $note = factory(Note::class)->make([
            'project_id' => $this->project->id,
            'deadline' => '2019-12-12'
        ]);

        $validator = Validator::make($note->toArray(), $createNoteRequest->rules(), $createNoteRequest->messages());

        $this->assertFalse($validator->fails());

    }

    public function testProjectIdValidationFails()
    {
        $createNoteRequest = new createNoteRequest();

        $note = factory(Note::class)->make([
            'project_id' => $this->project->id + 1,
            'deadline' => '2019-12-12'
        ]);

        $validator = Validator::make($note->toArray(), $createNoteRequest->rules(), $createNoteRequest->messages());

        $this->assertTrue($validator->fails());

    }

    public function testNoteTitleFails()
    {
        $createNoteRequest = new createNoteRequest();

        $note = factory(Note::class)->make([
            'project_id' => $this->project->id,
            'title' => str_random(2),
            'deadline' => '2019-12-12'
        ]);

        $validator = Validator::make($note->toArray(), $createNoteRequest->rules(), $createNoteRequest->messages());

        $this->assertTrue($validator->fails());
    }

    public function testContentFails()
    {
        $createNoteRequest = new createNoteRequest();

        $note = factory(Note::class)->make([
            'project_id' => $this->project->id,
            'content' => str_random(2),
            'deadline' => '2019-12-12'
        ]);

        $validator = Validator::make($note->toArray(), $createNoteRequest->rules(), $createNoteRequest->messages());

        $this->assertTrue($validator->fails());
    }

    public function testSharedWithFails()
    {
        $createNoteRequest = new createNoteRequest();

        $note = factory(Note::class)->make([
            'project_id' => $this->project->id,
            'shared_with' => ["Not an email"],
            'deadline' => '2019-12-12'
        ]);

        $validator = Validator::make($note->toArray(), $createNoteRequest->rules(), $createNoteRequest->messages());

        $this->assertTrue($validator->fails());
    }

    public function testSharedWithSucceess()
    {
        $createNoteRequest = new createNoteRequest();


        $note = factory(Note::class)->make([
            'project_id' => $this->project->id,
            'shared_with' => [$this->user->email],
            'deadline' => '2019-12-12'
        ]);

        $validator = Validator::make($note->toArray(), $createNoteRequest->rules(), $createNoteRequest->messages());

        $this->assertFalse($validator->fails());
    }

    public function testDeadlineIncorrectFormatFailure()
    {
        $createNoteRequest = new createNoteRequest();


        $note = factory(Note::class)->make([
            'project_id' => $this->project->id,
            'deadline' => '2019-12-12 15:30'
        ]);

        $validator = Validator::make($note->toArray(), $createNoteRequest->rules(), $createNoteRequest->messages());

        $this->assertFalse($validator->fails());
    }

    public function testLastModifiedByIncorrectFormat()
    {
        $createNoteRequest = new createNoteRequest();


        $note = factory(Note::class)->make([
            'project_id' => $this->project->id,
            'deadline' => '2019-12-12',
            'last_modified_by' => 'abc'
        ]);

        $validator = Validator::make($note->toArray(), $createNoteRequest->rules(), $createNoteRequest->messages());

        $this->assertTrue($validator->fails());
    }

}
