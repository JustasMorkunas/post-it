<?php
/**
 * Created by PhpStorm.
 * User: Justas
 * Date: 2019-11-14
 * Time: 18:24
 */
declare(strict_types=1);


namespace resourceMicroservice\tests\Unit\Handlers\Requests;


use App\Http\Requests\UserRequests\loginRequest;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Tests\TestCase;

class LoginRequestTest extends TestCase
{

    public const PASSWORD = "testPass";

    private $user;

    public function setUp()
    {
        parent::setUp();
        $this->user = factory(User::class)->make([
            'password' => Hash::make(self::PASSWORD)
        ]);

        $this->user->save();
    }

    public function tearDown()
    {
        parent::tearDown();
        $this->user->delete();
    }

    /**
     * @dataProvider provider
     */
    public function testAllValidationFailures($email, $password) {
        $loginRequest = new loginRequest();

        $user = [
            'email' => $email,
            'password' => $password,
        ];

        $validator = Validator::make($user, $loginRequest->rules(), $loginRequest->messages());

        $this->assertTrue($validator->fails());
    }

    public function provider() {
        return [
            ['notAnEmail.com', self::PASSWORD,],
            ['notFound@notAnEmail.com', self::PASSWORD,],
        ];
    }


    public function testPasswordNotEntered() {
        $loginRequest = new loginRequest();

        $user = [
            'email' => $this->user->email
        ];

        $validator = Validator::make($user, $loginRequest->rules(), $loginRequest->messages());

        $this->assertTrue($validator->fails());
    }

    public function testIncorrectEmailFormat() {
        $loginRequest = new loginRequest();

        $user = [
            'email' => 'notAnEmail.com',
            'password' => self::PASSWORD,
        ];

        $validator = Validator::make($user, $loginRequest->rules(), $loginRequest->messages());

        $this->assertTrue($validator->fails());
    }

    public function testEmailNotFoundInDatabase() {
        $loginRequest = new loginRequest();

        $user = [
            'email' => 'notFound@notAnEmail.com',
            'password' => self::PASSWORD,
        ];

        $validator = Validator::make($user, $loginRequest->rules(), $loginRequest->messages());

        $this->assertTrue($validator->fails());
    }

    public function testIfValidationPassesWithCorrectData() {
        $loginRequest = new loginRequest();

        $user = [
            'email' => $this->user->email,
            'password' => self::PASSWORD,
        ];

        $validator = Validator::make($user, $loginRequest->rules(), $loginRequest->messages());

        $this->assertFalse($validator->fails());
    }
}
