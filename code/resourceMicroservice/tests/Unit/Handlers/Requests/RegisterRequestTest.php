<?php
/**
 * Created by PhpStorm.
 * User: Justas
 * Date: 2019-11-14
 * Time: 18:39
 */
declare(strict_types=1);


namespace resourceMicroservice\tests\Unit\Handlers\Requests;


use App\Http\Requests\UserRequests\registerRequest;
use App\User;
use Illuminate\Support\Facades\Validator;
use Tests\TestCase;

class RegisterRequestTest extends TestCase
{

    private function getUserArray()
    {
        return [
            'name' => str_random(6),
            'lastname' => str_random(7),
            'email' => 'testemail@test.com',
            'password' => 'pass',
            'password_confirm' => 'pass',
        ];
    }

    public function testNoNameFailure()
    {
        $registerRequest = new registerRequest();

        $user = $this->getUserArray();

        unset($user['name']);

        $validator = Validator::make($user, $registerRequest->rules(), $registerRequest->messages());

        $this->assertTrue($validator->fails());
    }

    public function testNoLastNameFailure()
    {
        $registerRequest = new registerRequest();

        $user = $this->getUserArray();

        unset($user['lastname']);

        $validator = Validator::make($user, $registerRequest->rules(), $registerRequest->messages());

        $this->assertTrue($validator->fails());
    }

    public function testNoEmailFailure()
    {
        $registerRequest = new registerRequest();

        $user = $this->getUserArray();

        unset($user['email']);

        $validator = Validator::make($user, $registerRequest->rules(), $registerRequest->messages());

        $this->assertTrue($validator->fails());
    }

    public function testBadEmailFormatFailure()
    {
        $registerRequest = new registerRequest();

        $user = $this->getUserArray();

        $user['email'] = 'notAEmail.com';

        $validator = Validator::make($user, $registerRequest->rules(), $registerRequest->messages());

        $this->assertTrue($validator->fails());
    }

    public function testTestNotUniqueEmailFailure()
    {
        $registerRequest = new registerRequest();

        $existingUser = factory(User::class)->make([]);
        $existingUser->save();

        $user = $this->getUserArray();
        $user['email'] = $existingUser->email;

        $validator = Validator::make($user, $registerRequest->rules(), $registerRequest->messages());

        $this->assertTrue($validator->fails());
        $existingUser->delete();
    }

    public function testNoPasswordFailure()
    {
        $registerRequest = new registerRequest();

        $user = $this->getUserArray();

        unset($user['password']);

        $validator = Validator::make($user, $registerRequest->rules(), $registerRequest->messages());

        $this->assertTrue($validator->fails());
    }

    public function testNoPasswordConfirmationFailure()
    {
        $registerRequest = new registerRequest();

        $user = $this->getUserArray();

        unset($user['password_confirm']);

        $validator = Validator::make($user, $registerRequest->rules(), $registerRequest->messages());

        $this->assertTrue($validator->fails());
    }

    public function testNotMatchingPasswordConfirmationFailure()
    {
        $registerRequest = new registerRequest();

        $user = $this->getUserArray();

        $user['password_confirm'] .= 'a';

        $validator = Validator::make($user, $registerRequest->rules(), $registerRequest->messages());

        $this->assertTrue($validator->fails());
    }


    public function testIfValidationPassesWithCorrectData()
    {
        $registerRequest = new registerRequest();

        $user = $this->getUserArray();

        $validator = Validator::make($user, $registerRequest->rules(), $registerRequest->messages());

        $this->assertFalse($validator->fails());
    }

}
